var config = {};
config.database = {};
config.databaseCons = {};
config.cookie = {};

config.database.user = 'postgres';
config.database.host = 'localhost';
config.database.database = 'postgres';
config.database.password = '0904';
config.database.poolPort = '5432';
config.database.port = '3003';
config.database.origin = 'http://localhost:3000';

config.cookie.key = 'userID';
config.cookie.secret = 'learnfromonline';

config.databaseCons.port = '3001';

module.exports = config;
require('dotenv').config();

const Pool = require('pg').Pool;

// console.log(process.env.DB_PORT);

const pool = new Pool({
    user: process.env.DB_USER,
    host: 'db',
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
});

const query_command_user ="DROP SCHEMA public CASCADE;\
                        CREATE SCHEMA public;\
                        CREATE TABLE if not exists normal_user( \
                         userid serial, \
                         teamid int[], \
                         roleid int[] , \
                         full_name varchar(30),\
                         birth date,\
                         preferred_name varchar(30),\
                         phone_number char(20),\
                         email varchar(20),\
                         emergencyId int,\
                         primary key(userid)\
                     );\
\                   create table if not exists user_info(\
                            userid serial primary key,\
                            email varchar(255),\
                            password varchar(255),\
                            FK_userid int unique,\
                            foreign key(FK_userid) references normal_user(userid)\
                            );\
                            \
                     CREATE TABLE if not exists admin_coordinator( \
                         userid serial, \
                         teamid int [], \
                         roleid int [], \
                         full_name varchar(30),\
                         birth date,\
                         preferred_name varchar(30),\
                         phone_number char(20),\
                         email varchar(20),\
                         emergencyId int,\
                         constraint PK_adminID primary key (userID)\
                       );\
\
                     CREATE TABLE if not exists role(  \
                            roleID int not null primary key, \
                            roleName varchar(20)\
                     );\
\
                    create table if not exists emergency_contactRole( \
                         emergencyId serial, \
                         name varchar(10) , \
                         familyRole varchar(10) , \
                         phone_number char(10) , \
                         primary key (emergencyId)\
                    ); \
\
                    CREATE TABLE if not exists wwcc_number( \
                               number char(10) not null primary key, \
                               APP_number char(20) not null, \
                               type varchar(20) , \
                               expiry_date date, \
                               valid_status int default 0\
                        ); \
                        CREATE TABLE if not exists volunteer( \
                         userid serial, \
                         teamid int [], \
                         roleid int [], \
                         full_name varchar(30),\
                         birth date,\
                         preferred_name varchar(30),\
                         phone_number char(20),\
                         email varchar(20),\
                         emergencyId int,\
                        training_model int default 0, \
                        number_wwcc char(10), \
                        foreign key(number_wwcc) references wwcc_number(number), \
                        constraint PK_userID primary key (userID) \
                         ); \
                         \
                         CREATE TABLE if not exists team_coordinator ( \
                         userid serial, \
                         teamid int[], \
                         roleid int[] , \
                         full_name varchar(30),\
                         birth date,\
                         preferred_name varchar(30),\
                         phone_number char(10),\
                         work_number char(10),\
                         home_number char(10),\
                         email varchar(20),\
                         emergencyId int,\
                         constraint PK_teamCoordinator_userID primary key (userID), \
                        number_wwcc char(10), \
                        foreign key(number_wwcc) references wwcc_number(number) \
                    ); \
\
                    CREATE TABLE if not exists team_leader ( \
                       userid serial, \
                         teamid int[], \
                         roleid int[] , \
                         full_name varchar(30),\
                         birth date,\
                         preferred_name varchar(30),\
                         phone_number char(20),\
                         email varchar(20),\
                         emergencyId int,\
                        training_model int default 0, \
                        orderID int[], \
                        incidentID int[], \
                        constraint PK_teamLeader_userID primary key (userID) \
                    ) ;\
\
                    CREATE TABLE if not exists homeless_record( \
                                 recordID serial, \
                                 legal_name varchar(20), \
                                 incidentID int[], \
                                 street_name varchar(20), \
                                 birth_year int, \
                                 registered_date date, \
                                 blanket_number int, \
                                 bags_number int, \
                                 adminid int not null,\
                                 foreign key(adminid) references admin_coordinator(userid),\
                                 primary key (recordID)\
                    ); \
\
                    CREATE TABLE if not exists normal_order ( \
                        orderID serial primary key, \
                        status varchar(10),\
                        userID int ,\
                        homeless_record_id int ,\
                        blanket_number int, \
                        bags_number int, \
                        ordered_date date,\
                        received_date date, \
                        cloth varchar(255),\
                        special_request varchar(255),\
                        comments varchar(255),\
                        foreign key(homeless_record_id) references homeless_record(recordID),\
                        foreign key(userID) references team_leader(userID) \
                    ); \
\
                    CREATE TABLE if not exists incident ( \
                        incidentID serial primary key, \
                        userID int ,\
                        status varchar(10), \
                        time time, \
                        date date, \
                        engaged_activity varchar(40), \
                        location varchar(30), \
                        description varchar(100), \
                        recipient varchar(30),\
                        reporter varchar(30), \
                        reported_time time, \
                        reported_date date, \
                        foreign key(userID)  references team_leader(userID) \
                        ); \
\
                    CREATE TABLE if not exists team ( \
                         teamId serial, \
                         teamName varchar(10), \
                         teamLeaderId int [],\
                         eventId int[], \
                         teamCoordinatorId int[] ,\
                         volunteerid int[],\
                         contactInformation varchar(100),\
                         group_name varchar(100),\
                         car_type varchar(10),\
                         primary key (teamId)\
                        ); \
\
                    CREATE TABLE if not exists event( \
                         eventID serial, \
                         teamID int ,\
                         foreign key(teamID) references team(teamID), \
                         incidentId int[], \
                         team_name varchar(10), \
                         team_night varchar(10), \
                         start_time time, \
                         end_time time, \
                         event_name varchar(30), \
                         date date, \
                         volunteerid int [],\
                         driver varchar(20), \
                         kitchen_condition_report varchar(200), \
                         Vehicle_condition_report varchar(200), \
                         Night_comment varchar(200), \
                         PRIMARY KEY (eventId) \
                    ); \
\
                    CREATE TABLE if not exists location( \
                         locationID serial,  \
                         eventId int, \
                         Num_of_female int, \
                         Num_of_male int, \
                         Num_of_child int, \
                         total int, \
                         PRIMARY KEY(locationId,eventID), \
                         FOREIGN KEY(eventId) REFERENCES event(eventId) \
                            ); \
                        \
                    CREATE OR REPLACE FUNCTION insertUsers()\
                    returns trigger as\
                    $body$\
                    declare\
                    roleids int array;\
                    begin\
                     select roleid into roleids from normal_user where userid = new.userid;\
                    if 1 = any(roleids) then\
                    insert into volunteer\
                    values(new.userid, new.teamid, new.roleid, new.full_name, new.birth, new.preferred_name, new.phone_number,new.email, new.emergencyid, 0, null);\
                    end if;\
                    \
                    if 2 = any(roleids) then\
                    insert into admin_coordinator\
                    values(new.userid, new.teamid, new.roleid, new.full_name, new.birth, new.preferred_name, new.phone_number,new.email, new.emergencyid);\
                    end if;\
                    if 3 = any(roleids) then\
                    insert into team_coordinator(userid, teamid, roleid, full_name, birth, preferred_name, phone_number,email, emergencyid, number_wwcc)\
                    values(new.userid, new.teamid, new.roleid, new.full_name, new.birth, new.preferred_name, new.phone_number,new.email, new.emergencyid, null);\
                    end if;\
                    if 4 = any(roleids) then\
                    insert into team_leader (userid, teamid, roleid, full_name, birth, preferred_name, phone_number,email, emergencyid, training_model)\
                    values(new.userid, new.teamid, new.roleid, new.full_name, new.birth, new.preferred_name, new.phone_number,new.email, new.emergencyid, 0);\
                    end if;\
                    return new;\
                    end;\
                    $body$ language plpgsql;\
                    \
                    CREATE TRIGGER  addUsers\
                    AFTER INSERT\
                    ON normal_user\
                    FOR EACH ROW\
                    EXECUTE PROCEDURE insertUsers(); \
                    \
                    CREATE OR REPLACE FUNCTION updateWWCC()\
                      RETURNS trigger AS\
                    $$\
                    BEGIN\
                             update team_coordinator set number_wwcc = (NEW.number_wwcc)\
                             where userid = NEW.userid;\
                     \
                        RETURN NEW;\
                    END;\
                    $$\
                    LANGUAGE 'plpgsql';\
                    \
                    create trigger updateWWCC\
                    after update\
                    on volunteer\
                    for each row\
                    EXECUTE PROCEDURE updateWWCC();\
                    \
                    insert into role\
                    values (1, 'Volunteer'), (2,'Admin'), (3, 'Team Coordinator'), (4,'Team Leader');\
                    \
                    insert into emergency_contactRole (name, familyrole, phone_number)\
                    values('emergency1','father','043333432'),('emergency2','mother','043233432'),\
                    ('emergency3','son','043333409'),('emergency4','uncle','040333432'),('emergency5','daughter','043953432'),('mike','father','043953432'),('tiffany','mother','043953234'),\
                    ('scofield','brother','043953654'),('connie','sister','043953347'),('bonnie','sister','043953346'),('jackson','brother','043953335'),\
                    ('jackie','uncle','043953264'),('tom','grandPa','043953835'),('jerry','brother','043953724'),('isabella','grandMa','043953473'),\
                    ('tim','son','043953373'),('bill','uncle','043953737'),('bella','daughter','043953758'),('sandra','sister','043953853');\
                    insert into normal_user(teamid,roleid,full_name,birth,preferred_name,phone_number,email,emergencyid) values('{1}','{1,2,3,4}','super tom','2000-02-02','Tom','0412455','123@123.com',null),\
                    ('{2}','{1,3}','Coordinator/volunteer Jon','1994-12-13','Jon','0456341242','Jon@gmail.com',null),\
                    ('{1}','{1,3}','Coordinator M','1994-12-13','M','0456341242','M@gmail.com',null),\
                    ('{1}','{1,3}','Coordinator K','1994-12-13','K','0456341222','K@gmail.com',null),\
                    ('{2}','{1,2,3}','Admin/team coordinator Arnott','2000-01-09','Arnott','044412390','Arnott@gamil.com',null),\
                    (null, '{1,2}','Admin Jupyter','1989-01-01','Jupyter','043245390','Jupyter@gamil.com',5),\
                    ('{1}','{1,2,3,4}','allRoles Han','1990-10-09','LH','0444234590','han@gamil.com',null),\
                    ('{2}','{1,4}','Leader Lam','1994-10-09','LH','0444253590','Lam@gamil.com',3),\
                    (null,'{1,2,4}','Admin/Leader Mike','1994-11-09','LH','0444253735','Mike@gamil.com',6),\
                    (null,'{1,3,4}','Admin/Team Coordinator Tom','1994-12-09','LH','0444253362','Tom@gamil.com',7),\
                    ('{3}','{1,2}','Admin Mark','1994-10-27','MI','0444253732','Mark@gamil.com',8),\
                    ('{3,6}','{1,2}','Admin Lilly','1994-10-16','LI','0444253470','Lilly@gamil.com',9),\
                    ('{4,7}','{1,2}','Admin Teemo','1994-10-18','Tee','0443623590','Teemo@gamil.com',10),\
                    ('{3,5}','{1,3}','team coordinator Jinx','1994-10-15','JI','0444735590','Jinx@gamil.com',11),\
                    ('{3,2}','{1,3}','team coordinator Jerry','1994-10-14','JE','0446243590','Jerry@gamil.com',12),\
                    ('{4}','{1,3}','team coordinator Lux','1994-10-17','LU','0444734590','Lux@gamil.com',13),\
                    ('{3,7}','{1,4}','Leader Oliver','1994-10-14','LO','0444453590','Oliver@gamil.com',14),\
                    ('{4}','{1,4}','Leader Zoe','1994-10-18','Z','0444272590','Zoe@gamil.com',15),\
                    ('{5}','{1,4}','Leader Olaf','1994-10-14','Olaf','0447423590','Olaf@gamil.com',16),\
                    ('{5,6}','{1,2,3}','Admin/team coordinator Yasuo','1994-10-10','SUO','0447273590','Yasuo@gamil.com',17),\
                    ('{6,7}','{1,3,4}','team coordinator/Leader Theo','1953-10-09','T','0444326590','Theo@gamil.com',18),\
                    ('{7}','{1,2,4}','Admin/Leader Malphite','1994-10-09','Stone','0444723590','Malphite@gamil.com',19);\
                    insert into wwcc_number values('000000001','10000000','type1','2025-10-10'),('000000002','10000000','primary','2025-10-10'),\
                    ('000000003','10000000','secondary','2024-09-10'),('000000004','10000000','primary','2022-10-27'),('000000005','10000001','secondary','2024-09-27'),\
                    ('000000006','10000002','secondary','2024-09-23'),('000000007','10000003','primary','2024-09-17'),('000000008','10000004','high','2024-09-06'),\
                    ('000000009','10000005','secondary','2024-09-28'),('000000010','10000006','primary','2024-09-19'),('000000011','10000007','high','2024-09-02');\
                    insert into incident (userid, status, time, date, engaged_activity, location, description, recipient, reporter,reported_time, reported_date)\
                    values (7,'unfinished','10:00:01','2021-09-03','activity1','location1','description1','recipient1','reporter1','19:09:01','2021-10-04'),\
                    (8,'done','18:00:01','2021-05-03','activity2','location3','description5','recipient1','reporter4','14:06:01','2021-05-04'),\
                    (7,'finished','10:06:01','2021-09-23','activity6','location2','description1','recipient1','reporter1','04:09:01','2021-10-09'),\
                    (7,'submitted','11:16:00','2021-10-05','activity','great sydney','no description','admin A','coordinator jon','12:40:31','2021-11-30'),\
                    (8,'rejected','23:14:00','2021-10-13','help homeless people','Pitt st','no description'                ,'admin A','coordinator jon','09:30:25','2021-04-02'),\
                    (17,'submitted','12:14:00','2021-10-27','give food','Queen st','Here are some homeless people hungry...','Admin/Leader Malphite',       'team coordinator Jinx','09:36:25','2021-04-30'),\
                    (17,'rejected','22:14:00','2021-10-11','give clothes','Quay st','no description'                        ,'Admin Lilly',                 'team coordinator Jinx','13:30:25','2021-04-30'),\
                    (17,'submitted','21:14:00','2021-10-14','give water','Queen st','We meet two homeless people...'        ,'Admin Teemo',                 'team coordinator Jerry','16:30:25','2021-04-16'),\
                    (18,'rejected','17:14:00','2021-10-27','give water','Quay st','no description'                          ,'Admin Mark',                  'team coordinator Lux','23:30:25','2021-04-25'),\
                    (18,'submitted','14:14:00','2021-10-15','help homeless people','Queen st','no description'              ,'Admin/team coordinator Yasuo','team coordinator Lux','06:30:25','2021-04-27'),\
                    (18,'rejected','18:14:00','2021-10-18','give clothes','Pitt st','We meet two homeless people...'        ,'Admin Mark',                  'team coordinator Lux','04:30:25','2021-04-02'),\
                    (19,'submitted','21:14:00','2021-09-13','give food','Quay st','Here are some homeless people hungry...' ,'Admin/Leader Mike',           'Admin/team coordinator Yasuo','02:30:25','2021-04-27'),\
                    (19,'rejected','18:14:00','2021-09-30','give water','Queen st','no description'                         ,'Admin/Team Coordinator Tom',  'Admin/team coordinator Yasuo','08:30:25','2021-04-13'),\
                    (21,'submitted','22:14:00','2021-09-06','give clothes','Pitt st','We meet two homeless people...'       ,'Admin Teemo',                 'Admin/team coordinator Yasuo','14:30:25','2021-04-26'),\
                    (21,'rejected','06:14:00','2021-09-08','give food','Quay st','Here are some homeless people hungry...'  ,'Admin/Leader Malphite',       'Admin/team coordinator Yasuo','11:30:25','2021-04-16');\
                    insert into homeless_record (legal_name, incidentID, street_name, birth_year, registered_date, blanket_number, bags_number, adminid) \
                    values('homeless1','{5,11}','Pitt st',1999,'2021-04-19',2,3,6),\
                    ('homeless2','{5}','Pitt st',           1994,'2021-05-19',0,3,6),\
                    ('homeless3','{11,14}','Pitt st',       1995,'2021-01-13',1,3,5),\
                    ('homeless4','{5,14}','Pitt st',        1996,'2021-01-26',1,1,9),\
                    ('homeless5','{5,11,15}','Pitt st',     1997,'2021-01-18',1,2,9),\
                    ('homeless6','{6,8}','Queen st',        1998,'2021-01-07',1,1,11),\
                    ('homeless7','{8,10,13}','Queen st',      1991,'2021-01-28',1,1,12),\
                    ('homeless8','{6,8,10}','Queen st',     1994,'2021-02-13',1,4,13),\
                    ('homeless9','{6,8,10,13}','Queen st',  1994,'2021-06-13',1,2,9),\
                    ('homeless10','{7,9,12}','Quay st',     1996,'2021-09-13',1,1,11),\
                    ('homeless11','{7,12,15}','Quay st',    1999,'2021-11-13',1,9,9),\
                    ('homeless12','{7,9,12,15}','Quay st',  1993,'2021-01-14',2,2,22),\
                    ('homeless13','{12,15}','Quay st',      1995,'2021-01-17',1,1,20);\
                    insert into normal_order (status, userID, homeless_record_id, blanket_number, bags_number, ordered_date, received_date, cloth,special_request, comments)\
                    values('processing',7,1,10,20,'2020-10-20','2020-10-21','clothing1','no request','no comments'),\
                    ('processing',8,2,50,20,'2020-09-20','2020-10-21','clothing2','no request','no comments'),\
                    ('submitted',7,3,10,20,'2020-10-20','2020-10-21','clothing5','no request','no comments'),\
                    ('rejected',7,3,1,200,'2021-01-26','2021-09-12','clothing with','request with burger','more sauce'),\
                    ('approved',8,1,14,0,'2021-11-02','2021-12-19','clothing with jacket','request with surgeon','more blanket'),\
                    ('rejected',17,4,14,10, '2021-11-26','2021-12-15','clothing with jacket','request with surgeon','more blanket'),\
                    ('approved',8,5,4,0,    '2021-11-06','2021-12-19','clothing with sweater','request with burger','more bag'),\
                    ('rejected',17,6,14,10, '2021-11-03','2021-12-14','clothing with jacket','request with surgeon','more blanket'),\
                    ('approved',18,7,1,0,   '2021-11-07','2021-12-14','clothing with sweater','request with surgeon','more blanket'),\
                    ('approved',19,8,14,10, '2021-11-02','2021-12-12','clothing with jacket','request with burger','more bag'),\
                    ('rejected',21,9,13,0,  '2021-11-08','2021-12-17','clothing with jacket','request with surgeon','more blanket'),\
                    ('approved',18,10,15,10,'2021-11-02','2021-12-14','clothing with sweater','request with burger','more bag'),\
                    ('approved',21,11,1,0,  '2021-11-07','2021-12-12','clothing with jacket','request with surgeon','more blanket'),\
                    ('rejected',19,12,14,6, '2021-11-03','2021-12-14','clothing with jacket','request with burger','more bag'),\
                    ('approved',18,13,4,0,  '2021-11-02','2021-12-17','clothing with sweater','request with surgeon','more blanket'),\
                    ('approved',22,10,5,4,  '2021-11-09','2021-12-13','clothing with jacket','request with burger','more blanket'),\
                    ('rejected',17,11,7,10, '2021-11-02','2021-12-12','clothing with jacket','request with surgeon','more bag'),\
                    ('approved',22,12,14,0, '2021-11-05','2021-12-18','clothing with sweater','request with game','more blanket'),\
                    ('approved',18,13,9,10, '2021-11-02','2021-12-13','clothing with jacket','request with surgeon','more bag');\
                    \
                    insert into team(teamName,teamLeaderId,eventId,teamCoordinatorId,volunteerid,contactInformation,group_name,car_type) values ('team A','{1}',null,'{3}','{1}','contact information','independent','BUS'),\
                    ('team B','{2}',null,'{4}','{1}','contact information','independent','TRUCK'),\
                    ('team C','{17}','{4,5}','{14,15}','{11,12}','contact information','independent','BUS'),\
                    ('team D','{18}','{6,7}','{16}','{13}','contact information','independent','TRUCK'),\
                    ('team E','{19}','{8}','{20}','{14}','contact information','independent','BUS'),\
                    ('team F','{21}','{9}','{20}','{12}','contact information','independent','TRUCK'),\
                    ('team G','{22}',null,'{21}','{13,17}','contact information','independent','TRUCK');\
                    insert into event(teamID,incidentId,team_name,team_night,start_time,end_time,date,volunteerid,driver,kitchen_condition_report,Vehicle_condition_report,Night_comment,event_name)  values\
                    (1,null,'teamA','nightA','00:00:00','00:00:01','2021-10-14','{4,7,9}','Sid','kitchen condition report1','vehicle report2','night comment 1','Monday daytime shift A'),\
                    (1,null,'teamA','night A','04:00:00','05:00:01','2021-10-14','{4,7,9}','Jackson','kitchen condition report2','vehicle report1','night comment 1','Monday daytime shift B'),\
                    (2,null,'teamB','night B','00:10:20','00:50:01','2021-10-23','{3,8,9}','June','kitchen condition report3','vehicle report3','night comment 3','Monday night shift A'),\
                    (3,'{6,7}','teamC','night C','04:46:00','05:16:01','2021-10-04','{4,5}','Mike','kitchen condition report4','vehicle report4','night comment 4','Monday night shift B'),\
                    (3,'{8}','teamC','night D','04:36:00','05:37:01','2021-10-15','{4,5}','Melisa','kitchen condition report5','vehicle report5','night comment 5','Thursday shift A'),\
                    (4,'{9,10}','teamD','night E','04:47:00','05:25:01','2021-10-04','{6,7}','Lem','kitchen condition report6','vehicle report6','night comment 6','Thursday  shift C'),\
                    (4,'{11}','teamD','night F','04:34:00','05:07:01','2021-10-27','{6,7}','Jackson','kitchen condition report7','vehicle report7','night comment 7','Sunday daytime shift A'),\
                    (5,'{12,13}','teamE','night G','04:32:00','05:24:01','2021-10-16','{8}','Jimmy','kitchen condition report8','vehicle report8','night comment 8','Sunday night shift F'),\
                    (6,'{14,15}','teamF','night H','04:28:00','05:35:01','2021-10-09','{9}','Sid','kitchen condition report9','vehicle report9','night comment 9','Sunday night shift H');\
                    insert into location(eventId,Num_of_female,Num_of_male,Num_of_child,total) values(1,3,5,2,10),(2,4,5,2,11),(3,3,50,2,55),(4,13,50,2,65),(5,3,40,2,45),(6,3,50,12,65),\
                    (7,3,30,2,35),(8,3,20,2,25),(9,13,10,2,25);\
                    \
                    update volunteer\
                    set number_wwcc= case userid\
                    when 8 then '000000005'\
                    when 13 then '000000006'\
                    when 14 then '000000007'\
                    when 16 then '000000008'\
                    when 2 then '000000009'\
                    when 1 then '000000010'\
                    when 7 then '000000011'\
                    else number_wwcc\
                    end\
                    where userid in (8,13,14,16,2,1,7);\
                    \
                    \
                        \
";

pool.query(query_command_user, (err, res) => {console.log(err, res);});
import React, {Component} from 'react';
import logo from '../image/logo.png';
import '../App.css';
import TeamLeaderMain from "./TeamLeaderMain.js";
import TeamLeaderRestored from './TeamLeaderRestored.js';
import CompleteTrainingModel from "./CompleteTrainingModel";
import {FormControl, InputLabel, MenuItem, Select} from "@mui/material";
import {useHistory} from "react-router-dom";
import AccountMain from "../account/AccountMain";
export let currentComponent = <TeamLeaderMain />;

export const TeamLeaderBanner = (props) => {
    let history = useHistory();
        return (
            <div>
                <header role="banner">
                    <div className="container">
                        <div className="brand">
                            <h2>
                                <a href="/">
                                    <img id='logo' src={logo} alt="Logo"/>
                                </a>
                            </h2>
                        </div>
                        <nav>
                            <div className="nav_wrapper">
                                <ul>
                                    {/*All top components*/}
                                    <li>
                                        <a onClick={() => {
                                            currentComponent = <TeamLeaderRestored userId={props.userId}/>;
                                            props.setComponent();
                                        }}>Rostered Patrol</a>
                                    </li>
                                    <li>
                                        <a onClick={() =>{
                                            currentComponent = <CompleteTrainingModel userId={props.userId}/>;
                                            props.setComponent();
                                        }}>Training Module</a>
                                    </li>
                                    <li>
                                        <a>Portal</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        {/*Navigation bar*/}
                        <nav role="navigation">
                            <ul>
                                <li>
                                    <a href="">Module</a>
                                </li>
                                <li>
                                    <a href="">History</a>
                                </li>

                                <li className="highlight">
                                    <a onClick={() => {
                                        currentComponent = <AccountMain userId={props.userId}/>;
                                        props.setComponent();
                                    }}>Account</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <br />
                </header>
                <br />
                <div>
                    <FormControl style={{marginLeft:'80%'}}>
                        <InputLabel id="TeamSelectLabel">Roles</InputLabel>
                        <Select
                            required
                            labelId="role-select-label"
                            id="role-select"
                            value={props.currentRole}
                            label="Roles ID"
                            onChange={(e) => {
                                if(e.target.value === 1){
                                    history.push("/volunteerController", {userId: props.userId, roleId:props.roleId,  currentRoleId:e.target.value});
                                }else if(e.target.value === 2){
                                    history.push('/admin', {userId: props.userId, roleId:props.roleId,  currentRoleId:e.target.value});
                                }else if(e.target.value === 3){
                                    history.push("/teamCoordinatorController", {userId: props.userId, roleId:props.roleId, currentRoleId:e.target.value});
                                }
                            }}
                        >
                            {props.roleId.map((eachId) => <MenuItem value={eachId}>{eachId}</MenuItem>)}
                        </Select>
                    </FormControl>
                </div>
            </div>
        );
}

export default TeamLeaderBanner;
import React from "react";
import TeamLeaderBanner from "./TeamLeaderBanner";
import './TeamLeaderMain.css';
import NewsSlides from "../newsSlides/NewsSlides";

//The main page component
const LeaderMain = () =>{
    return (
        <div className= "App">
            <h1 className={"title"}>Team Leader Portal</h1>
            {/*slides show component*/}
            <NewsSlides />
        </div>
    );
}


export default LeaderMain;
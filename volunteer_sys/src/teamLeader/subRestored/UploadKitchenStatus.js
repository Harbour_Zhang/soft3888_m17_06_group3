import React, {useEffect, useState} from "react";
import './All.css'
import {useHistory} from "react-router-dom";
import UserCheck from "../../security/UserCheck";
const UploadKitchenStatus = () =>{

    let history = useHistory();
    useEffect(() => {
        UserCheck().then(res => {
            if(!res){
                history.push('/login');
            }
        });
    },[])

    return (
        <div>
            <p>Upload Image</p>
            <form action = '' method = "get" id = "all" className={''}>
                <div className={'inputOne'}>
                    <label>Kitchen Image:</label>
                    <input type = "file" id = 'kitchenImage' />
                </div>

                <div className={'inputOne'}>
                    <label>Vehicle Image:</label>
                    <input type = "file" id = 'vehicleImage' />
                </div>

            </form >

            <button type = "submit" form = "all" value = "Submit" className={"submit"}>Upload</button>
        </div>

    )

}

export default UploadKitchenStatus;
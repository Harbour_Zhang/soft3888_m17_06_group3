import React, {useEffect, useState} from "react";
import './All.css'
import Axios from "axios";
import {FormControl, InputLabel, MenuItem, Select, TextField} from "@mui/material";
import Box from "@mui/material/Box";
import SaveIcon from "@mui/icons-material/Save";
import SendIcon from "@mui/icons-material/Send";
import {LoadingButton} from "@mui/lab";
const CreateOrderRequest = (props) =>{

    const [blanket, setBlanket] = useState();
    const [bag, setBag] = useState();
    const [orderDate, setOrderDate] = useState();
    const [receiveDate, setReceiveDate] = useState();
    const [specialRequest, setSpecialRequest] = useState();
    const [comments, setComments] = useState();
    const [status, setStatus] = useState();
    const [cloth, setCloth] = useState();
    const [loading, setLoading] = React.useState(false);


    const [hl, sethl] = useState();
    const [hls, sethls] = useState();
    const [hlid, sethlid] = useState();

    useEffect(() =>{
        Axios.get('http://localhost:3003/admin/view_record').then((response) =>{
            const homeless = response.data.recordList.map((eachName) => eachName.legal_name);
            sethls(
                <FormControl style={{width :'40%'}}>
                    <InputLabel id="hlsSelectLabel">Homeless Record</InputLabel>
                    <Select
                        required
                        labelId="hl-name-select-label"
                        id="hl-name-select"
                        value={hl}
                        label="Homeless legal name"
                        onChange={(e) => {
                            sethl(e.target.value);
                            let list = response.data.recordList;
                            for (let i=0;i <list.length; i++){
                                if (list[i].legal_name === e.target.value){
                                    sethlid(list[i].recordid);
                                }
                            }
                            // getTeamMembers(e.target.value);
                        }}
                    >
                        {homeless.map((eachName) => <MenuItem value={eachName}>{eachName}</MenuItem>)}
                    </Select>
                </FormControl>
            );
        })
    }, [hl])
    function sendInfo(){

        Axios.post('http://localhost:3003/teamLeader/createOrder',{
            blank: blanket,
            bag: bag,
            orderedDate: orderDate,
            receivedDate: receiveDate,
            request: specialRequest,
            comment: comments,
            status: status,
            cloth:cloth,
            userid: props.userId,
            homelessid :hlid,
        }).then((response) => {
            console.log(response.data);
        }, (error) => {
            console.log(error);
        });
    }





    return (
        <div>
            <p>Order Info:</p>
            <div id = "all" className={'mainInput'}>
                <div>
                    {hls}
                </div>


                <Box
                    component="form"
                    sx={{
                        '& > :not(style)': { m: 1, width: '25ch',height:'10ch' },
                    }}
                    noValidate
                    autoComplete="off"
                >
                
                    <TextField label="Number of Blank" type = 'number' onChange={(e) => {
                        setBlanket(e.target.value);
                    }} focused />
                
                    <TextField label="Number of Bag" type = 'number' onChange={(e) => {
                        setBag(e.target.value);
                    }} focused />
                    <TextField label="Cloth" type = 'text' onChange={(e) => {
                        setCloth(e.target.value);
                    }} focused />
                </Box>

                <Box
                    component="form"
                    sx={{
                        '& > :not(style)': { m: 1, width: '25ch',height:'10ch' },
                    }}
                    noValidate
                    autoComplete="off"
                >
                    <TextField label="Order Date" type = 'date'  onChange={(e) => {
                        setOrderDate(e.target.value);
                    }}focused />
             

                
                    <TextField label="Receive Date" type = 'date' onChange={(e) => {
                        setReceiveDate(e.target.value);
                    }} focused />
                
                </Box>

                <Box
                    component="form"
                    sx={{
                        '& > :not(style)': { m: 1, width: '25ch',height:'10ch' },
                    }}
                    noValidate
                    autoComplete="off"
                >
                    <TextField label="Special Request"  onChange={(e) => {
                        setSpecialRequest(e.target.value);
                    }}focused />
                    <TextField label="Comments" onChange={(e) => {
                        setComments(e.target.value);
                    }} focused />
         
                    <TextField label="Status" onChange={(e) => {
                        setStatus(e.target.value);
                    }} focused />
                </Box>  
             


            </div>

            <LoadingButton
                onClick={sendInfo}
                endIcon={<SendIcon />}
                loading={loading}
                loadingPosition="end"
                variant="contained"
            >
                Submit
            </LoadingButton>
        </div>

    )

}

export default CreateOrderRequest;
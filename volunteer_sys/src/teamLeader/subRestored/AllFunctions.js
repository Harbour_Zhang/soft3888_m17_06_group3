import React, {useState} from "react";
import CreateIncident from './CreateIncident.js';
import CreateOrderRequest from "./CreateOrderRequest.js";
import UploadKitchenStatus from './UploadKitchenStatus';
import {Button} from "@mui/material";
import Box from "@mui/material/Box";

// Manage the team members and add team members component
// Not fully implement yet
const AllFunctions = (props) => {
    let currentType = 'Choose Category';
    const [Content,setContent] = useState(<div>Select One Task.</div>);


    function switchToIncident(){
        currentType = 'Create Incident';
        console.log(props.userId);
        setContent(<CreateIncident userId = {props.userId}/> );
    }

    function switchToOrder() {
        currentType = 'Create Order Requests';
        setContent(<CreateOrderRequest userId = {props.userId}/>);
    }

    function switchToKitchenStatus() {
        currentType = 'Upload Kitchen and vehicle status';
        setContent((<UploadKitchenStatus userId = {props.userId}/>));
    }

    return (
        <div className= "App">
           <div >
            <div className='TaskList'>
                <Box
                    component="form"
                    sx={{
                        '& > :not(style)': { m: 1, width: '20%',height:'10ch'},
                    }}
                    noValidate
                    autoComplete="off"
                >
                    <Button variant="contained" onClick ={switchToIncident}>Create Incident</Button>
                    <Button variant="contained" onClick={switchToOrder}>Create Order Requests</Button>
                    <Button variant="contained" onClick={switchToKitchenStatus}>Upload Kitchen and vehicle status</Button><br/><br/>
                </Box>
            </div>
            <div className='ActualTask'>
                {Content}
            </div>
           </div>

        </div>

    );
}

export default AllFunctions;
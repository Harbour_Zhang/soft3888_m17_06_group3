import React, {useEffect, useState} from "react";
import './All.css'
import Axios from "axios";
import {FormControl, InputLabel, MenuItem, Select, TextField} from "@mui/material";
import Box from "@mui/material/Box";
import SaveIcon from "@mui/icons-material/Save";
import {LoadingButton} from "@mui/lab";
import SendIcon from "@mui/icons-material/Send";
const CreateIncident = (props) =>{

    const [engagedActivity, setEngagedActivity] = useState();
    const [location, setLocation] = useState();
    const [recipient, setRecipient] = useState();
    const [reporter, setReporter] = useState();
    const [date, setDate] = useState();
    const [time, setTime] = useState();
    const [reportedTime, setReportedTime] = useState();
    const [reportedDate, setReportedDate] = useState();
    const [allEvent, setAllEvent] = useState();
    const [event, setEvent] = useState();
    const [loading, setLoading] = React.useState(false);
    useEffect(() =>{
        Axios.get('http://localhost:3003/teamLeader/getAllEvent').then((response) =>{
            const events = response.data.list.map((eachName) => eachName.event_name);
            setAllEvent(
                <FormControl style={{width :'40%'}}>
                    <InputLabel id="eventSelectLabel">Event Name</InputLabel>
                    <Select
                        required
                        labelId="event-name-select-label"
                        id="event-name-select"
                        value={event}
                        label="Event Name"
                        onChange={(e) => {
                            setEvent(e.target.value);
                        }}
                    >
                        {events.map((eachName) => <MenuItem value={eachName}>{eachName}</MenuItem>)}
                    </Select>
                </FormControl>
            );
        });
    },[event]);

    function sendInfo(){
        Axios.post('http://localhost:3003/teamLeader/createIncident',{

            teamLeaderId:props.userId,
            engagedActivity:engagedActivity,
            location:location,
            recipient:recipient,
            reporter:reporter,
            date:date,
            time:time,
            reportedTime:reportedTime,
            reportedDate:reportedDate,
            event_name:event,
        }).then((response) => {
            console.log(response.data);
        }, (error) => {
            console.log(error);
        });


    }
    return (
        <div>
            <p>Incident Info</p>
            <div id = "all" className={'mainInput'}>
                <div>
                    {allEvent}
                </div>
                <Box
                    component="form"
                    sx={{
                        '& > :not(style)': { m: 1, width: '20%',height:'10ch'},
                    }}
                    noValidate
                    autoComplete="off"
                >
                <TextField label="Engaged Activity" onChange={(e) => {
                    setEngagedActivity(e.target.value);
                }}  focused />

                <TextField label="Location" onChange={(e) => {
                    setLocation(e.target.value);
                }}  focused />

                </Box>

                <Box
                    component="form"
                    sx={{
                        '& > :not(style)': { m: 1, width: '20%',height:'10ch'},
                    }}
                    noValidate
                    autoComplete="off"
                >
                        <TextField label="Recipient" onChange={(e) => {
                            setRecipient(e.target.value);
                        }}  focused />
                        <TextField label="Reporter"  onChange={(e) => {
                            setReporter(e.target.value);
                        }} focused />


                </Box>
                <Box
                    component="form"
                    sx={{
                        '& > :not(style)': { m: 1, width: '20%',height:'10ch'},
                    }}
                    noValidate
                    autoComplete="off"
                >
                    <TextField label="Time" type = 'time'type = 'time'  onChange={(e) => {
                        setTime(e.target.value);
                    }}  focused />
                    <TextField label="Date" type = 'date'type = 'date'  onChange={(e) => {
                        setDate(e.target.value);
                    }}  focused />
                </Box>

                <Box
                    component="form"
                    sx={{
                        '& > :not(style)': { m: 1, width: '20%',height:'10ch'},
                    }}
                    noValidate
                    autoComplete="off"
                >

                        <TextField label="Reported Time"  type = 'time' onChange={(e) => {
                            setReportedTime(e.target.value);
                        }}  focused />
                        <TextField label="Reported Date" type = 'date'  onChange={(e) => {
                            setReportedDate(e.target.value);
                        }} focused />


                </Box>



            </div>
            <LoadingButton
                onClick={sendInfo}
                endIcon={<SendIcon />}
                loading={loading}
                loadingPosition="end"
                variant="contained"
            >
                Submit
            </LoadingButton>
        </div>

    )

}

export default CreateIncident;
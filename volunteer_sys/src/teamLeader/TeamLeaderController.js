import React, {useEffect, useState} from "react";
import TeamLeaderBanner, {currentComponent} from "./TeamLeaderBanner";
import TeamLeaderMain from "./TeamLeaderMain";
import {useHistory} from "react-router-dom";
import UserCheck from "../security/UserCheck";


// Control all things happen in TeamCoordinator pages, make no url jump needed
export const TeamLeaderController = (props) =>{
    const [showComponent, setShowComponent] = useState(<TeamLeaderMain/>);
    const userId = props.history.location.state?.userId;
    const roleId = props.history.location.state?.roleId;
    const currentRoleId = props.location.state?.currentRoleId;

    function setComponent(){
        setShowComponent(currentComponent);
    }

    let history = useHistory();
    useEffect(() => {
        UserCheck().then(res => {
            if(!res){
                history.push('/login');
            }
        });
    },[])

    return (
        <div className= "App">
            <TeamLeaderBanner setComponent={setComponent} userId={userId} roleId={roleId} currentRole={currentRoleId}/>
            <div>{showComponent}</div>
        </div>
    );
}


export default TeamLeaderController;
import Axios from "axios";
import {useEffect, useState} from "react";


const CompleteTrainingModel = (props) =>{
    function sendInfo(){
        Axios.post('http://localhost:3003/teamLeader/finishedTraining', {
            teamid : props.userId
        }).then((response)=>{
            console.log(response.data);
        },(error) => {
            console.log(error);
        })
    }
    const [status, setStatus] = useState();

    useEffect(()=>{
        Axios.post('http://localhost:3003/teamLeader/getTrainingStatus', {
            teamid : props.userId
        }).then((response) =>{
            if (response.data.status === 0){
                setStatus('unfinished');
            }else{
                setStatus('Finished');
            }

        },(error) => {
            console.log(error);
        })
    }, [status]);

    return (

        <div>
            <p >Your Training Module Status: {status} </p>
            <br/>

            <a  href='https://www.volunteeringaustralia.org/wp-content/files_mf/1377052716VaGuidetotrainingvolunteerspartA.pdf' onClick={sendInfo}  >Click Here to Complete Training Module</a>

        </div>


    )
}

export default CompleteTrainingModel;
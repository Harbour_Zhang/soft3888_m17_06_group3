import React, {useEffect, useState} from "react";
import Volunteers, {selectedVolunteer, volunteerRows} from './Volunteers.js';
import './TeamLeaderMain.css';
import CreateOrderRequest from "./subRestored/CreateOrderRequest";
import Axios from "axios";
import {Button, FormControl, InputLabel, MenuItem, Select, TextField} from "@mui/material";
import SaveIcon from '@mui/icons-material/Save';
import Box from "@mui/material/Box";
import {useHistory} from "react-router-dom";
import AllFunctions from './subRestored/AllFunctions.js';
import {LoadingButton} from "@mui/lab";

export let currentComponent ;

const TeamLeaderRestored = (props) =>{
    let currentPage = 'Please Choose One Tasks';

    const [teamName, setTeamName] = useState('0');
    const [teamId, setTeamId] = useState('0');
    const [allTeamName,setAllTeamName] =  useState(<div></div>);
    const [leaderName, setLeaderName] = useState(<div></div>);
    const [driver, setDriver] = useState('0');
    const [drivers, setDrivers] = useState(<div></div>);
    const [currentVolunteer, setCurrentVolunteer] = useState();
    const [allVolunteer, setAllVolunteer] = useState(<div></div>);
    const [open, setOpen] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    // let volunteerRows;
    const [volunteerRows, setVolunteerRows] = useState();
    const [tn, setTN] = useState();
    const [nc, setNC] = useState();
    const [startTime, setStartTime] = useState();
    const [endTime, setEndTime] = useState();
    const [date, setDate] =  useState();
    const [eventName, setEventName] =  useState();


    useEffect(() =>{

        Axios.get('http://localhost:3003/teamLeader/getAllTeams').then((response) => {
            const teamNames = response.data.recordList.map((eachName) => eachName.teamname);

            setAllTeamName(
                <FormControl style={{width: '20%', color: '#004a99'}}>
                    <InputLabel id="TeamNameSelectLabel" className='smallComponent'>Team Name</InputLabel>
                    <Select
                        required
                        labelId="team-name-select-label"
                        id="team-name-select"
                        value={teamName}
                        label="Team Name"
                        onChange={(e) => {
                            setTeamName(e.target.value);
                            let list = response.data.recordList;
                            for (let i=0;i <list.length; i++){
                                if (list[i].teamname === e.target.value){
                                    setTeamId(list[i].teamid);
                                }
                            }
                            // getTeamMembers(e.target.value);
                        }}
                    >
                        {teamNames.map((eachName) => <MenuItem value={eachName}>{eachName}</MenuItem>)}
                    </Select>

                </FormControl>
            );

        });

        Axios.get('http://localhost:3003/allDrivers').then((response)=>{
            const drivers = response.data.list.map((eachName) => eachName.driver);
            setDrivers(
                <FormControl style={{width :'20%'}}>
                    <InputLabel id="DriverSelectLabel">Driver </InputLabel>
                    <Select
                        required
                        labelId="driver-name-select-label"
                        id="driver-name-select"
                        value={driver}
                        label="Driver "
                        onChange={(e) => {
                            setDriver(e.target.value);
                            // getTeamMembers(e.target.value);
                        }}
                    >
                        {drivers.map((eachName) => <MenuItem value={eachName}>{eachName}</MenuItem>)}
                    </Select>
                </FormControl>
            );
        });

        Axios.post('http://localhost:3003/teamLeader', { userId: props.userId}).then((response)=>{
            const value = response.data.fullName;
            setLeaderName(
                <Box
                component="form"
                sx={{
                    '& > :not(style)': { m: 1, width: '20%',color:'#004a99' },
                }}
                noValidate
                autoComplete="off"
            >
                <TextField label="Team Leader" value={value}  />
                <TextField label="Event Name" onChange={(e) => {
                    setEventName(e.target.value);
                }} focused />
            </Box>
        );
        });


    }, [teamName, driver, leaderName,allVolunteer]);




    function sendInfo(){
        Axios.post('http://localhost:3003/teamLeader/createEvent', {
            teamid : teamId,
            event_name: eventName,
            team_name: teamName,
            team_night : tn,
            start_time : startTime,
            end_time: endTime,
            date : date,
            night_comment : nc,
            volunteerid: selectedVolunteer,
            driver: driver,
        }).then((response) =>{
            console.log(response.data);
        },(error) => {
            console.log(error);
        })
    }





    return (
        <div className='App'>
            <h1>Fill the paper for new event</h1>
            <div className='Basic'>
                <h3>Basic Information</h3>
                <div>
                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': { m: 1, width: '20%', color:'#004a99' },
                        }}
                        noValidate
                        autoComplete="off"
                    >
                    {allTeamName}
                    {drivers}
                    </Box>
                </div>

                <div>

                    {leaderName}

                </div>

                <div>

                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': { m: 1, width: '20%', color:'#004a99' },
                        }}
                        noValidate
                        autoComplete="off"
                    >
                        <TextField label="Team Night" id = 'tn' onChange={(e) => {
                            setTN(e.target.value);
                        }} focused />
                        <TextField label="Night Comment" id = 'nc' onChange={(e) => {
                            setNC(e.target.value);
                        }}focused />
                    </Box>
                </div>
                <div>
                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': { m: 1, width: '20%', color:'#004a99' },
                        }}
                        noValidate
                        autoComplete="off"
                    >
                        <TextField label="Date" type='date' id='date'onChange={(e) => {
                            setDate(e.target.value);
                        }} focused />
                        <TextField label="Start Time" type='time' id='st' onChange={(e) => {
                            setStartTime(e.target.value);
                        }}focused />
                        <TextField label="End Time" type='time' id='et' onChange={(e) => {
                            setEndTime(e.target.value);
                        }} focused />

                    </Box>
                </div>

            </div>
            <div className='volunteers'>
                <h3>Volunteers Selection</h3>
                <Volunteers volunteers={volunteerRows} />
            </div>
            <br/><br/><br/><br/>
            <div>
                <LoadingButton
                    size='large'
                    onClick={sendInfo}
                    loading={loading}
                    loadingPosition="start"
                    startIcon={<SaveIcon />}
                    variant="contained"
                >
                    Save
                </LoadingButton>
            </div>
            <br/><br/>

            <div className='functions'>
                <h3>Other Functions</h3>
                <AllFunctions userId = {props.userId}/>

            </div>

            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

        </div>


    );

}

export default TeamLeaderRestored;


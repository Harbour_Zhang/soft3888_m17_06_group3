import React from "react";
import renderer from 'react-test-renderer';
import AdminPeople from "../admin/AdminPeople";

it('Renders when nothing pass',  () => {
    const tree = renderer.create(<AdminPeople />).toJSON();
    expect(tree).toMatchSnapshot();
});

it('Renders when parameter pass',  () => {
    const tree = renderer.create(<AdminPeople userId={1}/>).toJSON();
    expect(tree).toMatchSnapshot();
});
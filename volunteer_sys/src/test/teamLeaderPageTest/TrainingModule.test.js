import { unmountComponentAtNode, render } from "react-dom";
import { act } from "react-dom/test-utils";
import axios from "axios";
import CreateOrderRequest from "../../teamLeader/subRestored/CreateOrderRequest";
import CompleteTrainingModel from "../../teamLeader/CompleteTrainingModel";


let container = null;

beforeEach(() => {
    // setup a DOM element as a render target
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    // cleanup on exiting
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it("renders training status", async () => {
    const fakeStatus = {
        training_model: 'finished'
    };

    const mockGet = jest.spyOn(axios, 'get');
    mockGet.mockImplementation((url) => {
        switch (url) {
            case 'http://localhost:3003/teamLeader/getTrainingStatus':
                return Promise.resolve({data: fakeStatus});
        }
    });

    await act(async () => {
        render(<CompleteTrainingModel userId = '1'/>, container);
    });

    expect(container.textContent).toBe(fakeStatus.training_model);


    // remove the mock to ensure tests are completely isolated
    mockGet.mockRestore();
});
import { unmountComponentAtNode, render } from "react-dom";
import { act } from "react-dom/test-utils";
import axios from "axios";
import CreateIncident from "../../teamLeader/subRestored/CreateIncident";

let container = null;
beforeEach(() => {
    // setup a DOM element as a render target
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    // cleanup on exiting
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it("renders event names", async () => {
    const fakeEventList = {
        eventList: [
            {'event_name' : 'Monday daytime shift A'},
            {'event_name' : 'Monday daytime shift A'},
            {'event_name' : 'Monday daytime shift B'},
            {'event_name' : 'Monday night shift A'},
            {'event_name' : 'Thursday  shift C'},
            {'event_name' : 'Sunday daytime shift A'}]

    };
    jest.spyOn(axios, "get").mockImplementation((url) =>{
        switch (url) {
            case 'http://localhost:3003/teamLeader/getAllEvent':
                return Promise.resolve({data: fakeEventList});
        }
    });

    // Use the asynchronous version of act to apply resolved promises
    await act(async () => {
        render(<CreateIncident />, container);
    });
    expect(container.textContent).toContain(fakeEventList.eventList[0].event_name);

    const button = container.querySelector('.clickOnMe');

    await act(() => {
        button.dispatchEvent(new MouseEvent("click", {bubbles: true}));
    });
    // remove the mock to ensure tests are completely isolated
    global.fetch.mockRestore();
});



import { unmountComponentAtNode, render } from "react-dom";
import { act } from "react-dom/test-utils";
import axios from "axios";
import CreateOrderRequest from "../../teamLeader/subRestored/CreateOrderRequest";

let container = null;
beforeEach(() => {
    // setup a DOM element as a render target
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    // cleanup on exiting
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it("renders homeless names", async () => {
    const fakeHomelessList = {
        Homeless: [
            {"legal_name": "homeless2",},
            {"legal_name": "homeless3",},
            {"legal_name": "homeless4",},
            {"legal_name": "homeless5",}]
    };

    const mockGet = jest.spyOn(axios, 'get');
    mockGet.mockImplementation((url) => {
        switch (url) {
            case 'http://localhost:3003/admin/view_record':
                return Promise.resolve({data: fakeHomelessList});
        }
    });

    await act(async () => {
        render(<CreateOrderRequest />, container);
    });

    expect(container.textContent).toContain(fakeHomelessList.Homeless[0].legal_name);

    const button = container.querySelector('.clickOnMe');

    await act(() => {
        button.dispatchEvent(new MouseEvent("click", {bubbles: true}));
    });

    // remove the mock to ensure tests are completely isolated
    mockGet.mockRestore();
});


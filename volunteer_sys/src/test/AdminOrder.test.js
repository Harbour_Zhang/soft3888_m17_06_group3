import React from "react";
import renderer from 'react-test-renderer';
import AdminOrder from "../admin/AdminOrder";

it('Renders when nothing pass',  () => {
    const tree = renderer.create(<AdminOrder />).toJSON();
    expect(tree).toMatchSnapshot();
});

it('Renders when parameter pass',  () => {
    const tree = renderer.create(<AdminOrder userId={1}/>).toJSON();
    expect(tree).toMatchSnapshot();
});
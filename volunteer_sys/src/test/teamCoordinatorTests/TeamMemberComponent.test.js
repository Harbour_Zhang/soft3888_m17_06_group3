import React from "react";
import renderer from 'react-test-renderer';
import TeamMemberComponent from "../../teamCoordinator/TeamMemberComponent";

it('renders with nothing passed', () => {
    const tree = renderer.create(<TeamMemberComponent />);
    expect(tree).toMatchSnapshot();
});

it('renders with parameters', () => {
    const tree = renderer.create(<TeamMemberComponent type={'See Members'} userId={3}/>);
    expect(tree).toMatchSnapshot();
});
import React from "react";
import renderer from 'react-test-renderer';
import AddMember from "../teamCoordinator/AddMember";

it('Renders when nothing pass',  () => {
    const tree = renderer.create(<AddMember />).toJSON();
    expect(tree).toMatchSnapshot();
});

it('Renders when parameter pass',  () => {
    const tree = renderer.create(<AddMember type={"Add Member"} userId={1}/>).toJSON();
    expect(tree).toMatchSnapshot();
});
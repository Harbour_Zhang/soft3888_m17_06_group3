import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import PastRecords from "../../teamCoordinator/PastRecords";
import axios from "axios";

let container = null;

beforeEach(() => {
    // setup a DOM element as a render target
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    // cleanup on exiting
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it("renders user data", async () => {
    const fakeOrderList = {
        orderList: [{
            "orderid": 1,
            "status": "approved",
            "bags_number": 0,
            "blanket_number": 14,
            "cloth": "clothing with jacket",
            "comments": "more blanket",
            "homeless_record_id": 1,
            "ordered_date": "2021-11-02T00:00:00.000Z",
            "received_date": "2021-12-19T00:00:00.000Z",
            "special_request": "request with surgeon",
            "userid":1,
        }, {
            "orderid": 2,
            "status": "rejected",
            "bags_number": 0,
            "blanket_number": 14,
            "cloth": "clothing with jacket",
            "comments": "more blanket",
            "homeless_record_id": 1,
            "ordered_date": "2021-11-02T00:00:00.000Z",
            "received_date": "2021-12-19T00:00:00.000Z",
            "special_request": "request with surgeon",
            "userid":2,
        },]
    };


    const mockGet = jest.spyOn(axios, 'post');
    mockGet.mockImplementation((url) => {
        switch (url) {
            case 'http://localhost:3003/teamCoordinator/getAllOrders':
                return Promise.resolve({allRows: fakeOrderList});
        }
    });

    await act(async () => {
        render(<PastRecords type={'Orders'}/>, container);
    });

    expect(container.textContent).toContain(fakeOrderList.orderList[0].orderid);

    const button = container.querySelector('.clickOnMe');

    act(() => {
        button.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    });

    // remove the mock to ensure tests are completely isolated
    mockGet.mockRestore();
});

it("renders user data", async () => {
    const fakeOrderList = {
        orderList: [{
            "incidentid": 1,
            "status": "approved",
            "description": 0,
            "reported_date": "2021-11-02T00:00:00.000Z",
            "reporter": "a",
            "location": "request with surgeon",
            "engaged_activity":'none',
            "recipient":'a'
        }, {
            "incidentid": 2,
            "status": "approved",
            "description": 0,
            "reported_date": "2021-11-02T00:00:00.000Z",
            "reporter": "a",
            "location": "request with surgeon",
            "engaged_activity":'none',
            "recipient":'a'
        },]
    };


    const mockGet = jest.spyOn(axios, 'post');
    mockGet.mockImplementation((url) => {
        switch (url) {
            case 'http://localhost:3003/teamCoordinator/getAllIncidents':
                return Promise.resolve({allRows: fakeOrderList});
        }
    });

    await act(async () => {
        render(<PastRecords type={'Orders'}/>, container);
    });

    expect(container.textContent).toContain(fakeOrderList.orderList[0].orderid);

    const button = container.querySelector('.clickOnMe');

    act(() => {
        button.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    });

    // remove the mock to ensure tests are completely isolated
    mockGet.mockRestore();
});
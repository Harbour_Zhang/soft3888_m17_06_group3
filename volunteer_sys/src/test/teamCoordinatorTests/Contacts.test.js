import React from "react";
import renderer from 'react-test-renderer';
import TeamCoordinatorContact from "../teamCoordinator/TeamCoordinatorContact";

it('Renders when nothing pass',  () => {
    const tree = renderer.create(<TeamCoordinatorContact />).toJSON();
    expect(tree).toMatchSnapshot();
});
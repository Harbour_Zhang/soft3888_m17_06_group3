import React, {Profiler, useEffect, useState} from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect,
    useHistory,
    useLocation
} from "react-router-dom";
import './App.css';

import Login, {loginStatusGlobal} from "./login/Login";
import Dashboard from "./dashboard/Dashboard";
import Register from './login/Register';

import TeamCoordinatorMain from './teamCoordinator/TeamCoordinatorMain.js';
import TeamCoordinatorPastEvents from './teamCoordinator/TeamCoordinatorPastEvents.js';
import TeamCoordinatorContact from './teamCoordinator/TeamCoordinatorContact.js';
import TeamCoordinatorManage from "./teamCoordinator/TeamManager";
import Volunteer_traningModule  from "./volunteer/Volunteer_traningModule";
import Volunteer_WWCC from "./volunteer/Volunteer_WWCC";
import Volunteer_history from "./volunteer/volunteer_history";
import TeamCoordinatorController from "./teamCoordinator/TeamCoordinatorController";
import TeamLeaderController from "./teamLeader/TeamLeaderController";
import TeamLeaderMain from './teamLeader/TeamLeaderMain.js';
import createIncident from './teamLeader/subRestored/CreateIncident.js'
import VolunteerController from "./volunteer/VolunteerController";
import VolunteerMain from "./volunteer/VolunteerMain";
// import AdminController from './admin/AdminController.js';
// import App1 from './test/test.js';


import AdminController from './admin/AdminController';

import Account from "./account/AccountMain";
import AccountController from "./account/AccountController";

// import App1 from './test/test.js';


function App() {

    return (
        <Router>
            <Switch>
                <Route exact path={"/"} component={Login}/>
                <Route exact path={"/login"} component={Login}/>
                <Route exact path={"/register"} component={Register}/>
                <Route exact path={"/dashboard"} component={() => <Dashboard authorized={loginStatusGlobal} />} />
                <Route exact path={"/teamCoordinatorController"} component={TeamCoordinatorController} />
                <Route exact path={"/teamCoordinatorMain"} component = {TeamCoordinatorMain}/>
                <Route exact path={"/teamCoordinatorPastEvents"} component = {TeamCoordinatorPastEvents}/>
                <Route exact path={"/teamCoordinatorContact"} component = {TeamCoordinatorContact}/>
                <Route path={"/teamManage"} component={TeamCoordinatorManage}/>

                <Route exact path={"/volunteer/traningModule"} component={Volunteer_traningModule}/>
                <Route exact path={"/volunteer/WWCC"} component={Volunteer_WWCC}/>
                <Route exact path={"/volunteer/history"} component={Volunteer_history}/>
                <Route exact path={"/volunteerMain"} component={VolunteerMain}/>
                <Route exact path={"/volunteerController"} component={VolunteerController}/>

                <Route exact path={"/admin"} component = {AdminController}/>

                <Route exact path={"/teamLeaderController"} component = {TeamLeaderController}/>
                <Route exact path={"/teamLeaderMain"} component = {TeamLeaderMain}/>
                <Route exact path={"/teamLeader/createIncidentPage"} component = {createIncident}/>


                {/*<Route exact path={"/test"} component = {App1}/>*/}
                <Route exact path={"/account"} component={AccountController}/>

                {/*<Route exact path={"/test"} component = {App1}/>*/}
            </Switch>
        </Router>
    );
}

export default App;

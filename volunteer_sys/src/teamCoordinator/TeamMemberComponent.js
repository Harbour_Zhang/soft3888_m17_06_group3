import React, {useEffect, useState} from "react";
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import Axios from "axios";
import {FormControl, InputLabel, MenuItem, Select} from "@mui/material";
import ContactPopUp from "./ContactPopUp";
import UserCheck from "../security/UserCheck";
import {useHistory} from "react-router-dom";

function createData(userid, role, fullName, birthday, preferName, phone, email, emergencyId, wwccNumber) {
    return {
        preferName, role, email, phone, userid,fullName,birthday,wwccNumber,emergencyId,
    };
}

function Row(props) {
    const { row } = props;

    return (
        <React.Fragment>
            <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell component="th" scope="row">
                    {row.preferName}
                </TableCell>
                <TableCell>{row.role}</TableCell>
                <TableCell>{row.email}</TableCell>
                <TableCell>{row.phone}</TableCell>
                <TableCell>
                    <ContactPopUp userid={row.userid} fullName={row.fullName} birthday={row.birthday} wwccNumber={row.wwccNumber} emergencyId={row.emergencyId}/>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}


const TeamMemberComponent = (props) => {
    let history = useHistory();
    const [result,setResult] = useState(<div>Looking up in the database, please wait a bit.</div>);
    const [allTeamId, setAllTeamId] = useState(<div> </div>);
    const [teamid,setTeamId] = useState();

    function getTeamMembers(teamid){
        Axios.post('http://localhost:3003/getTeamMember', {
            teamId:teamid
        }).then((response) => {
            const allRows = response.data.allrows.map((eachRow) => createData(eachRow.userid, eachRow.roleid, eachRow.full_name, eachRow.birth, eachRow.preferred_name, eachRow.phone_number, eachRow.email, eachRow.emergencyid, eachRow.number_wwcc));
            setResult(<div>
                <TableContainer component={Paper}>
                    <Table aria-label="collapsible table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Prefer Name</TableCell>
                                <TableCell>Role</TableCell>
                                <TableCell>Email</TableCell>
                                <TableCell>Phone</TableCell>
                                <TableCell>More Info</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {allRows.map((row) => (
                                <Row key={row.userid} row={row} />
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>)
        })
    }

    const handleChange = (event) => {
        setTeamId(event.target.value);
        document.getElementById("team-select").selectedIndex = 0
        getTeamMembers(event.target.value);
    };

    useEffect(()=>{
        UserCheck().then(res => {
            if(!res){
                history.push('/login');
            }
        });
        if(props.type === 'See Members'){
            Axios.post('http://localhost:3003/teamCoordinator', {
                userId:props.userId
            }).then((response) => {
                const teamIds = response.data.teamId.map((eachId) => eachId);
                setTeamId(teamIds[0]);
                setAllTeamId(
                    <FormControl fullWidth>
                        <InputLabel id="TeamSelectLabel">Team ID</InputLabel>
                        <Select
                            required
                            labelId="team-select-label"
                            id="team-select"
                            value={teamid}
                            label="Team ID"
                            onChange={handleChange}
                        >
                            {teamIds.map((eachId) => <MenuItem value={eachId}>{eachId}</MenuItem>)}
                        </Select>
                    </FormControl>
                );
                Axios.post('http://localhost:3003/getTeamMember', {
                    teamId:response.data.teamId[0]
                }).then((response) => {
                    console.log(response.data);
                    const allRows = response.data.allrows.map((eachRow) => createData(eachRow.userid, eachRow.roleid, eachRow.full_name, eachRow.birth, eachRow.preferred_name, eachRow.phone_number, eachRow.email, eachRow.emergencyid, eachRow.number_wwcc));
                    setResult(<div>
                        <TableContainer component={Paper}>
                            <Table aria-label="collapsible table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Prefer Name</TableCell>
                                        <TableCell>Role</TableCell>
                                        <TableCell>Email</TableCell>
                                        <TableCell>Phone</TableCell>
                                        <TableCell>More Info</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {allRows.map((row) => (
                                        <Row key={row.userid} row={row} />
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>)
                })
            })
        }
    },[props.type])
    return (
        <div>
            <div>{allTeamId}</div>
            <br />
            <div>{result}</div>
        </div>
    );
}

export default TeamMemberComponent;
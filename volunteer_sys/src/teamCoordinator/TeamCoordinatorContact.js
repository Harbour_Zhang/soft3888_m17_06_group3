import React, {useEffect, useState} from "react";
import Axios from "axios";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import Paper from "@mui/material/Paper";
import {TablePagination} from "@mui/material";
import ContactPopUp from "./ContactPopUp";
import UserCheck from "../security/UserCheck";
import {useHistory} from "react-router-dom";

function createData(userid, teamid, role, fullName, birthday, preferName, phone, email, emergencyId, wwccNumber) {
    return {
        preferName, teamid, role, email, phone, userid,fullName,birthday,wwccNumber,emergencyId,
    };
}

function Row(props) {
    const { row } = props;
    return (
        <React.Fragment>
            <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell component="th" scope="row">
                    {row.preferName}
                </TableCell>
                <TableCell>{row.teamid}</TableCell>
                <TableCell>{row.role}</TableCell>
                <TableCell>{row.email}</TableCell>
                <TableCell>{row.phone}</TableCell>
                <TableCell>
                    <ContactPopUp userid={row.userid} fullName={row.fullName} birthday={row.birthday} wwccNumber={row.wwccNumber} emergencyId={row.emergencyId}/>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}

const TeamCoordinatorContact = () =>{
    let history = useHistory();

    const [result,setResult] = useState(<div>Looking up in the database, please wait a bit.</div>);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    useEffect(() => {
        UserCheck().then(res => {
            if(!res){
                history.push('/login');
            }
        });
        Axios.get('http://localhost:3003/teamCoordinator/getContacts').then((response) => {
            const allRows = response.data.allrows.map((eachRow) => createData(eachRow.userid, eachRow.teamid,eachRow.roleid, eachRow.full_name, eachRow.birth, eachRow.preferred_name, eachRow.phone_number, eachRow.email, eachRow.emergencyid, eachRow.number_wwcc));
            setResult(
                <div style={{'marginLeft':'10%', marginRight:'10%'}}>
                    <Paper sx={{ width: '100%', overflow: 'hidden' }}>
                        <TableContainer sx={{ maxHeight: 800 }}>
                            <Table stickyHeader aria-label="sticky table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Prefer Name</TableCell>
                                        <TableCell>Teams</TableCell>
                                        <TableCell>Roles</TableCell>
                                        <TableCell>Email</TableCell>
                                        <TableCell>Phone</TableCell>
                                        <TableCell>More Info</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {allRows
                                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                        .map((row) => {
                                            return (
                                                <Row key={row.userid} row={row} />
                                            );
                                        })}
                                </TableBody>
                            </Table>
                        </TableContainer>
                        <TablePagination
                            rowsPerPageOptions={[5,10, 25, 50]}
                            component="div"
                            count={allRows.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                        />
                    </Paper>
                </div>
            )
        })
    },[page,rowsPerPage])
    return (
        <div>
            <h1 className={"title"}>Team Coordinator Contacts</h1>
            {result}
        </div>
    );
};

export default TeamCoordinatorContact;
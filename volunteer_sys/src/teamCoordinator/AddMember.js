import React, {useEffect, useState} from "react";
import {
    Box,
    Button,
    Container,
    createTheme,
    CssBaseline,
    FormControl,
    Grid, InputLabel, MenuItem, Select,
    TextField,
    ThemeProvider
} from "@mui/material";
import Axios from "axios";
import Collapsible from "react-collapsible";
import Typography from "@mui/material/Typography";
import {waitFor} from "@testing-library/react";
import UserCheck from "../security/UserCheck";
import {useHistory} from "react-router-dom";

// Add member component (not implement yet)
const AddMember = (props) => {
    let history = useHistory();
    const [userid, setUserid] = useState('');
    const [fullName, setFullName] = useState('');
    const [teamid,setTeamId] = useState();
    const [replyMessage, setReplyMessage] = useState('');
    const [allTeamId, setAllTeamId] = useState(<div> </div>);
    const theme = createTheme();

    useEffect(()=> {
        UserCheck().then(res => {
            if(!res){
                history.push('/login');
            }
        });
        Axios.post('http://localhost:3003/teamCoordinator', {
            userId:props.userId
        }).then((response) => {
            const teamIds = response.data.teamId.map((eachId) => eachId);
            setAllTeamId(
                <FormControl fullWidth>
                    <InputLabel id="TeamSelectLabel">Team ID</InputLabel>
                    <Select
                        required
                        labelId="team-select-label"
                        id="team-select"
                        value={teamid}
                        label="Team ID"
                        onChange={(e) => {setTeamId(e.target.value)}}
                    >
                        {teamIds.map((eachId) => <MenuItem value={eachId}>{eachId}</MenuItem>)}
                    </Select>
                </FormControl>
            );
            setTeamId(response.data.teamId[0]);
        })
    },[props.type])

    function getReply(){
        if(userid === ''){
            setReplyMessage("Please input at least one credit.");
        }else{
            Axios.post('http://localhost:3003/teamCoordinator/addTeamMemberById', {
                teamId: teamid,
                fullName: fullName
            }).then((response) => {
                console.log(response);
                setReplyMessage(response.data);
            })
        }
    }


    return (
        <div>
            <div style={{color:'#004a99'}}>Please input the information.</div>
            <br />
            <div>
                <Box sx={{ flexGrow: 1 }}>
                    <Grid container spacing={1} rowGap={4} onSubmit={getReply} noValidate>
                        <Grid xs={6} md={12}>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="userid"
                                label="User ID"
                                name="userid"
                                autoFocus
                                onChange={(e) => {
                                    setUserid(e.target.value)
                                }}
                            />
                        </Grid>
                        <Grid xs={6} md={12}>
                            {allTeamId}
                        </Grid>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 5, mb: 12, height: 50 }}
                            style={{'backgroundColor':'#004a99'}}
                            onClick={getReply}
                        >
                            Add
                        </Button>
                        {replyMessage}
                    </Grid>
                </Box>
            </div>
        </div>
    )
}

export default AddMember;
import React, {useState} from "react";
import './TeamCoordinatorPastEvents.css';
import AddMember from "./AddMember";
import TeamMemberComponent from "./TeamMemberComponent";

// Manage the team members and add team members component
// Not fully implement yet
const TeamCoordinatorManage = (props) => {
    let currentType = 'Choose Category';
    const [sideContent,setSideContent] = useState(<div>Please choose a category.</div>);

    function changeTypeToMembers() {
        changeMembersColour();
        currentType = 'See Members';
        setSideContent(<TeamMemberComponent type={currentType} userId={props.userId}/>);
        document.getElementById("currentType").innerHTML = currentType;
    }

    function changeTypeToAdd() {
        changeAddColour();
        currentType = 'Add Member';
        setSideContent(<AddMember type={currentType} userId={props.userId}/>);
        document.getElementById("currentType").innerHTML = currentType;
    }

    function changeMembersColour(){
        let MembersButton = document.getElementById('MembersButton');
        let MembersColour = window.getComputedStyle(MembersButton).backgroundColor;
        if (MembersColour === 'rgb(0, 74, 153)') {
            document.getElementById('MembersButton').style.background = "#004a99";
            document.getElementById('MembersButton').style.color = '#ffffff';
        }
        document.getElementById('AddButton').style.backgroundColor = '#eee';
        document.getElementById('AddButton').style.color = "#004a99";
    }

    function changeAddColour() {
        let addButton = document.getElementById('AddButton');
        let addColour = window.getComputedStyle(addButton).backgroundColor;
        if (addColour === 'rgb(0, 74, 153)') {
            document.getElementById('AddButton').style.background = "#004a99";
            document.getElementById('AddButton').style.color = '#ffffff';
        }
        document.getElementById('MembersButton').style.backgroundColor = '#eee';
        document.getElementById('MembersButton').style.color = "#004a99";
    }

    return (
        <div className= "App">
            <div className={'box'}>
                <div className="sidenav">
                    <a id={'MembersButton'} onClick={changeTypeToMembers}>
                        <div className={'textPlaceHolder'}>Manage Members</div>
                    </a>
                    <a id={'AddButton'} onClick={changeTypeToAdd}>
                        <div className={'textPlaceHolder'}>Add Member</div>
                    </a>
                </div>
                <div className={'main'}>
                    <p className={'contentTitle'} id={'currentType'}>{currentType}</p>
                    <div className={'contentData'}>{sideContent}</div>
                </div>
            </div>

        </div>
    );
}

export default TeamCoordinatorManage;
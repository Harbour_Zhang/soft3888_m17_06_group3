import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

export const ContactPopUp = (props) => {
    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <Button variant="outlined" onClick={handleClickOpen}>
                More
            </Button>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {"Details"}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        User ID: {props.userid}
                    </DialogContentText>
                    <br />
                    <DialogContentText id="alert-dialog-description">
                        Full Name: {props.fullName}
                    </DialogContentText>
                    <br />
                    <DialogContentText id="alert-dialog-description">
                        Birth: {props.birthday}
                    </DialogContentText>
                    <br />
                    <DialogContentText id="alert-dialog-description">
                        WWCC Number: {props.wwccNumber}
                    </DialogContentText>
                    <br />
                    <DialogContentText id="alert-dialog-description">
                        Emergency ID: {props.emergencyId}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Close</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default ContactPopUp;
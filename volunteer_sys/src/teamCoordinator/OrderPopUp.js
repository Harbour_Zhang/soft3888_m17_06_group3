import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

export const OrderPopUp = (props) => {
    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <Button variant="outlined" onClick={handleClickOpen}>
                More
            </Button>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {"Details"}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        User ID: {props.userid}
                    </DialogContentText>
                    <br />
                    <DialogContentText id="alert-dialog-description">
                        Received Date: {props.received_date}
                    </DialogContentText>
                    <br />
                    <DialogContentText id="alert-dialog-description">
                        Blanket Num: {props.blanket_number}
                    </DialogContentText>
                    <br />
                    <DialogContentText id="alert-dialog-description">
                        Bags Num: {props.bags_number}
                    </DialogContentText>
                    <DialogContentText id="alert-dialog-description">
                        Cloth: {props.cloth}
                    </DialogContentText>
                    <DialogContentText id="alert-dialog-description">
                        Special Request: {props.special_request}
                    </DialogContentText>
                    <DialogContentText id="alert-dialog-description">
                        Comments: {props.comments}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Close</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default OrderPopUp;
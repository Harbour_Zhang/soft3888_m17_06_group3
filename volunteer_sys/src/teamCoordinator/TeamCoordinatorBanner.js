import React, {Component, useContext} from 'react';
import logo from '../image/logo.png';
import '../App.css';
import TeamCoordinatorController from "./TeamCoordinatorController";
import TeamCoordinatorManage from "./TeamManager";
import TeamCoordinatorMain from "./TeamCoordinatorMain";
import TeamCoordinatorContact from "./TeamCoordinatorContact";
import TeamCoordinatorPastEvents from "./TeamCoordinatorPastEvents";
import {FormControl, InputLabel, MenuItem, Select} from "@mui/material";
import {useHistory} from "react-router-dom";
import AccountMain from "../account/AccountMain";

export let currentComponent = <TeamCoordinatorMain />;

// Banner component, control all banner elements
export const TeamCoordinatorBanner = (props) => {
    let history = useHistory();
    return (
        <div>
            <header role="banner">
                <div className="container">
                    <div className="brand">
                        <h2>
                            <a href="/">
                                <img id='logo' src={logo} alt="Logo"/>
                            </a>
                        </h2>
                    </div>

                    <nav>
                        <div className="nav_wrapper">
                            <ul>
                                {/*All top components*/}
                                <li>
                                    <a onClick={() => {
                                        currentComponent = <TeamCoordinatorManage userId={props.userId}/>;
                                        props.setComponent();
                                    }}>Team manage</a>
                                </li>
                                <li>
                                    <a onClick={() => {
                                        currentComponent = <TeamCoordinatorContact />;
                                        props.setComponent();
                                    }}>Contacts</a>
                                </li>
                                <li>
                                    <a onClick={() => {
                                        currentComponent = <TeamCoordinatorPastEvents userId={props.userId}/>;
                                        props.setComponent();
                                    }}>Past Events</a>
                                </li>
                                <li>
                                    <a onClick={() => {
                                        currentComponent = <TeamCoordinatorMain />;
                                        props.setComponent();
                                    }}>Portal</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    {/*Navigation bar*/}
                    <nav role="navigation">
                        <ul>
                            <li>
                                <a href="">Module</a>
                            </li>
                            <li>
                                <a href="">History</a>
                            </li>

                            <li className="highlight">
                                <a onClick={() => {
                                    currentComponent = <AccountMain userId={props.userId}/>;
                                    props.setComponent();
                                }}>Account</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </header>
            <br />
            <div>
                <FormControl style={{marginLeft:'80%'}}>
                    <InputLabel id="TeamSelectLabel">Roles</InputLabel>
                    <Select
                        required
                        labelId="role-select-label"
                        id="role-select"
                        value={props.currentRole}
                        label="Roles ID"
                        onChange={(e) => {
                            if(e.target.value === 1){
                                history.push("/volunteerController", {userId: props.userId, roleId:props.roleId,  currentRoleId:e.target.value});
                            }else if(e.target.value === 2){
                                history.push('/admin', {userId: props.userId, roleId:props.roleId,  currentRoleId:e.target.value});
                            }else if(e.target.value === 4){
                                history.push("/teamLeaderController", {userId: props.userId, roleId:props.roleId,  currentRoleId:e.target.value});
                            }
                        }}
                    >
                        {props.roleId.map((eachId) => <MenuItem value={eachId}>{eachId}</MenuItem>)}
                    </Select>
                </FormControl>
            </div>
        </div>
    );
}

export default TeamCoordinatorBanner;

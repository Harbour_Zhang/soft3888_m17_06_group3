import React, {useEffect, useState} from "react";
import './TeamCoordinatorPastEvents.css';
import PastRecords from "./PastRecords";

//Past event component
//Control the content of the past order records or incidents records.
const TeamCoordinatorPastEvents = (props) =>{
    let currentType = 'Choose Category';

    const [pastRecords,setPastRecords] = useState(<div>Please choose a category.</div>);

    //Change the current type to incidents and update the content
    function changeTypeToIncidents() {
        changeIncidentsColour();
        currentType = 'Incidents Record';
        document.getElementById("currentType").innerHTML = currentType;
        let currentComponent = <PastRecords type={currentType} userId={props.userId} />;
        setPastRecords(currentComponent);
    }

    //Change the current type to orders and update content
    function changeTypeToOrders() {
        changeOrdersColour();
        currentType = 'Orders Record';
        document.getElementById("currentType").innerHTML = currentType;
        let currentComponent = <PastRecords type={currentType} userId={props.userId} />;
        setPastRecords(currentComponent);
    }

    function changeIncidentsColour(){
        let incidentsButton = document.getElementById('IncidentsButton');
        let incidentsColour = window.getComputedStyle(incidentsButton).backgroundColor;
        if (incidentsColour === 'rgb(0, 74, 153)') {
            document.getElementById('IncidentsButton').style.background = "#004a99";
            document.getElementById('IncidentsButton').style.color = '#ffffff';
        }
        document.getElementById('OrdersButton').style.backgroundColor = '#eee';
        document.getElementById('OrdersButton').style.color = "#004a99";
    }

    function changeOrdersColour() {
        let orderButton = document.getElementById('OrdersButton');
        let orderColour = window.getComputedStyle(orderButton).backgroundColor;
        if (orderColour === 'rgb(0, 74, 153)') {
            document.getElementById('OrdersButton').style.background = "#004a99";
            document.getElementById('OrdersButton').style.color = '#ffffff';
        }
        document.getElementById('IncidentsButton').style.backgroundColor = '#eee';
        document.getElementById('IncidentsButton').style.color = "#004a99";
    }


    return (
        <div className= "App">
            <div className={'box'}>
                <div className="sidenav">
                    <a id={'IncidentsButton'} onClick={changeTypeToIncidents}>
                        <div className={'textPlaceHolder'}>Incidents</div>
                    </a>
                    <a id={'OrdersButton'} onClick={changeTypeToOrders}>
                        <div className={'textPlaceHolder'}>Orders</div>
                    </a>
                </div>
                <div className={'main'}>
                    <p className={'contentTitle'} id={'currentType'}>{currentType}</p>
                    <div>{pastRecords}</div>
                </div>
            </div>

        </div>
    );
};

export default TeamCoordinatorPastEvents;
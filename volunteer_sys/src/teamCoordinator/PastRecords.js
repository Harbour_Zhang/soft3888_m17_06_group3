import React, {useEffect, useState} from "react";
import Axios from "axios";
import './Collapsible.css';
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import Paper from "@mui/material/Paper";
import IncidentsPopUp from "./IncidentsPopUp";
import {OrderPopUp} from "./OrderPopUp";
import UserCheck from "../security/UserCheck";
import {useHistory} from "react-router-dom";

// Past record component
// Distinguish which database should fetch by the 'type' prop

function createIncident(incidentId, description, status, reported_date, reporter,location, engaged_activity, recipient) {
    return {
        incidentId, status, reported_date, reporter,location, engaged_activity, recipient,
    };
}

function IncidentRow(props) {
    const { row } = props;
    const [open, setOpen] = useState(false);

    return (
        <React.Fragment>
            <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell component="th" scope="row">
                    {row.incidentId}
                </TableCell>
                <TableCell>{row.status}</TableCell>
                <TableCell>{row.reported_date}</TableCell>
                <TableCell>
                    <IncidentsPopUp reporter={row.reporter} location={row.location} engaged_activity={row.engaged_activity} recipient={row.recipient} />
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}

function createOrder(orderid, ordered_date, status, received_date, userid, blanket_number, bags_number, cloth, special_request, comments) {
    return {
        orderid, status, ordered_date,received_date, userid, blanket_number, bags_number, cloth, special_request, comments,
    };
}

function OrderRow(props) {
    const { row } = props;

    return (
        <React.Fragment>
            <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell component="th" scope="row">
                    {row.orderid}
                </TableCell>
                <TableCell>{row.status}</TableCell>
                <TableCell>{row.ordered_date}</TableCell>
                <TableCell>
                    <OrderPopUp userid={row.userid} received_date={row.received_date} blanket_number={row.blanket_number} bags_number={row.bags_number} cloth={row.cloth} special_request={row.special_request} comments={row.comments}/>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}


const PastRecords = (props) => {
    let history = useHistory();
    const [result,setResult] = useState(<div>Looking up in the database, please wait a bit.</div>);
    useEffect(() => {
        UserCheck().then(res => {
            if(!res){
                history.push('/login');
            }
        });
        if(props.type === 'Incidents Record'){
            Axios.post('http://localhost:3003/teamCoordinator/getAllIncidents' , {
                teamCoordinatorId: props.userId
            }).then((response) => {
                const allRows = response.data.allRows.map((eachRow) => createIncident(eachRow.incidentid, eachRow.description,eachRow.status, eachRow.reported_date, eachRow.reporter, eachRow.location, eachRow.engaged_activity, eachRow.recipient));
                setResult(<div style={{marginLeft:'10%', marginRight:'10%', color:'004a99'}}>
                    <TableContainer component={Paper}>
                        <Table aria-label="collapsible table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Incident ID</TableCell>
                                    <TableCell>Status</TableCell>
                                    <TableCell>Reported Date</TableCell>
                                    <TableCell>More Info</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {allRows.map((row) => (
                                    <IncidentRow key={row.incidentid} row={row} />
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>)
            })
        }else{
            Axios.post('http://localhost:3003/teamCoordinator/getAllOrders' , {
                teamCoordinatorId: props.userId
            }).then((response) => {
                const allRows = response.data.allRows.map((eachRow) => createOrder(eachRow.orderid, eachRow.ordered_date, eachRow.status, eachRow.received_date, eachRow.userid, eachRow.blanket_number,eachRow.bags_number,eachRow.cloth, eachRow.special_request, eachRow.comments));
                setResult(<div style={{marginLeft:'10%', marginRight:'10%', color:'004a99'}}>
                    <TableContainer component={Paper}>
                        <Table aria-label="collapsible table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Order ID</TableCell>
                                    <TableCell>Status</TableCell>
                                    <TableCell>Ordered Date</TableCell>
                                    <TableCell>More Info</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {allRows.map((row) => (
                                    <OrderRow key={row.orderid} row={row} />
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>)
            })
        }
    },[props.type])
    return (<div style={{'margin-left':'-20px'}} >{result}</div>);
}

export default PastRecords;
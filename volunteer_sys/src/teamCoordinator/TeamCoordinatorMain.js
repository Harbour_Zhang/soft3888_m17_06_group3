import React from "react";
import TeamCoordinatorBanner from "./TeamCoordinatorBanner";
import './TeamCoordinatorMain.css';
import NewsSlides from "../newsSlides/NewsSlides";

//The main page component
const CoordinatorMain = () =>{
    return (
        <div className= "App">
                <h1 className={"title"}>Team Coordinator Portal</h1>
                {/*slides show component*/}
                <NewsSlides />
        </div>
    );
}


export default CoordinatorMain;
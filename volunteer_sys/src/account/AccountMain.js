import React, {useEffect, useState} from "react";
import userImage from '../image/userImage.png'
import BasicInformation from "./subPart/BasicInformation";
import EmergencyContactRole from "./subPart/EmergencyContactRole";
import './Account.css';
import EditBasicInfo from "./subPart/EditBasicInfo";
import CreateWWCC from "./subPart/CreateWWCC";
import Axios from "axios";
import UserCheck from "../security/UserCheck";
import {useHistory} from "react-router-dom";
import LogOutPopUp from "./LogOutPopUp";


export const Account = (props) => {
    let history = useHistory();
    let currentPage = '';
    const [sideContent,setSideContent] = useState();

    function basicInformation () {
        currentPage = 'Basic Information';
        setSideContent(<BasicInformation userId={props.userId}/>);
        // document.getElementById("currentPage").innerHTML = currentPage;
    }


    function emergencyContactRole () {
        currentPage = 'Emergency Contact Role';
        setSideContent(<EmergencyContactRole userId={props.userId}/>);
        // document.getElementById("currentPage").innerHTML = currentPage;
    }

    function edit () {
        currentPage = 'Edit';
        setSideContent(<EditBasicInfo userId={props.userId}/>);
        //document.getElementById("currentPage").innerHTML = currentPage;
    }
    function createWWCC(){
        currentPage ='create WWCC';
        setSideContent(<CreateWWCC/>)
    }
    Axios.defaults.withCredentials = true;

    const [roleId,setRoleId]=useState("");
    useEffect(() => {
        UserCheck().then(res => {
            if(!res){
                history.push('/login');
            }
        });
        Axios.post('http://localhost:3003/user', {
            // userId: 7,
            userId: props.userId,
        }).then((response)=>{
            console.log(response)
            setRoleId(response.data.roleId);

        });
    },[]);



return(
        <div className='accountBox'>
            <h1 className={"title"}>User Profile</h1>
            <div className="sidenavAccount">

                <a onClick={basicInformation}>
                <div >Basic Information</div>
                </a>


                <a onClick={emergencyContactRole}>
                    <div >Emergency Contact Role</div>
                </a>

                <a onClick={edit}>
                    <div >Edit</div>
                </a>

                <a onClick={createWWCC}>
                    <div>
                        create WWCC number
                    </div>
                </a>


                <b id={'IogOutButton'}>
                    <LogOutPopUp />
                </b>

            </div>
            <img src={userImage}  alt="admin" width="100"/>
            <div className="selectedPart">
                <p>{currentPage}</p>
                <div>{sideContent}</div>
            </div>
        </div>
    );
}

export default Account;



import React, {Component, useContext} from 'react';
import logo from '../image/logo.png';
import '../App.css';

import AccountMain from "./AccountMain";

export let currentComponent = <AccountMain />;

// Banner component, control all banner elements
class AccountBanner extends Component {
    render() {
        return (
            <header role="banner">
                <div className="container">
                    <div className="brand">
                        <h2>
                            <a href="/">
                                <img id='logo' src={logo} alt="Logo"/>
                            </a>
                        </h2>
                    </div>

                    <nav>
                        <div className="nav_wrapper">
                        </div>
                    </nav>
                    {/*Navigation bar*/}
                    <nav role="navigation">
                        <ul>
                            <li className="highlight">
                                <a href="">Account</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </header>
        );
    }
}

export default AccountBanner;

import React, {useEffect, useState} from "react";
import "../Account.css"
import Axios from "axios";

function Information (props){
    return(
        <div className="Profile">
            <h4>
                {props.userInfo.fullName}
            </h4>
            <table>
                <tbody>
                <tr>
                    <th> User Id  </th>
                    <td> {props.userInfo.userId} </td>
                </tr>
                <tr>
                    <th> Team Id  </th>
                    <td> {props.userInfo.teamId?props.userInfo.teamId:"don't have team yet"}</td>
                </tr>
                <tr>
                    <th> Role Id </th>
                    <td> {props.userInfo.roleId}</td>
                </tr>
                <tr>
                    <th> PreferredName </th>
                    <td> {props.userInfo.preferredName}</td>
                </tr>
                <tr>
                    <th> PhoneNumber </th>
                    <td> {props.userInfo.phoneNumber}</td>
                </tr>
                <tr>
                    <th> Email </th>
                    <td> {props.userInfo.email}</td>
                </tr>
                <tr>
                    <th> Birth </th>
                    <td> {props.userInfo.birth}</td>
                </tr>
                <tr>
                    <th> WWCC Number  </th>
                    <td> {props.userInfo.wwccNumber?props.userInfo.wwccNumber:"don't have WWCC number yet"} </td>
                </tr>
                </tbody>
            </table>
        </div>

    )

}
const BasicInformation = (props) =>{


    Axios.defaults.withCredentials = true;
    const [userInfo, setUserInfo] = useState({});
    useEffect(() => {

        Axios.post('http://localhost:3003/volunteer', {
            // userId: 6,
            userId:props.userId,
        }).then((response)=>{
            setUserInfo({
                userId:response.data.userId,
                teamId:response.data.teamId,
                roleId:response.data.roleId,
                preferredName:response.data.preferredName,
                phoneNumber:response.data.phoneNumber,
                email:response.data.email,
                birth:response.data.birth,
                fullName:response.data.fullName,
                wwccNumber:response.data.wwccNumber,



            });

        });
    },[]);
    return(
        <div >
            <Information userInfo={userInfo}/>

        </div>

    )
}
export default BasicInformation;
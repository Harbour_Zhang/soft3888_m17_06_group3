import React, {useEffect, useState} from "react";
import "../Account.css"
import Axios from "axios";
function ContactRole(props){
    return(
        <div className="Profile">
            <table>
                <tbody>
                <tr>
                    <th> Name </th>
                    <td> {props.userInfo.name}</td>
                </tr>
                <tr>
                    <th> Family role  </th>
                    <td>  {props.userInfo.familyRole}</td>
                </tr>
                <tr>
                    <th> Phone number </th>
                    <td> {props.userInfo.phoneNumber}</td>
                </tr>
                </tbody>
            </table>
        </div>

    )

}
const EmergencyContactRole = (props) =>{
    Axios.defaults.withCredentials = true;
    const [userInfo, setUserInfo] = useState({});
    useEffect(() => {
        Axios.post('http://localhost:3003/emergencyContactRoleInAccount', {
            // userId: 8,
            userId:props.userId
        }).then((response)=>{
            setUserInfo({
                name:response.data.name,
                familyRole:response.data.familyRole,
                phoneNumber:response.data.phoneNumber,
            });

        });
    },[]);
    return(
        <div >
            <ContactRole userInfo={userInfo}/>

        </div>

    )
}
export default EmergencyContactRole;
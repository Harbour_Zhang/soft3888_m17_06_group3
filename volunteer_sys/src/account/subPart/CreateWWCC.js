import React, {useState} from "react";
import "../Account.css"
import Axios from "axios";

const CreateWWCC = (props) =>{

    Axios.defaults.withCredentials = true;
    const [createState, setCreateState] = useState("");
    const editInfo = () => {
        let app_number = document.getElementById('app_number').value;
        let type = document.getElementById('type').value;
        let expiry_date = document.getElementById('expiry_date').value;
        let wwccNumber = document.getElementById('wwccNumber').value;

        Axios.post('http://localhost:3003/createWWCCNumber', {
            wwccNumber:wwccNumber,
            app_number: app_number,
            type: type,
            expiry_date: expiry_date,

        }).then((response)=>{
            if (response.data == "success to create"){
                setCreateState(response.data)
            }else
            setCreateState(response.data.error.detail)
            console.log(response);
        }, (error) => {
            console.log(error);
            setCreateState("create failed")


        },[]);
    };

    return(
        <div >
            <div>
                <div>

                    <div className={'editInput'}>
                        <label >WWCC Number: </label>
                        <input type = 'text' id = 'wwccNumber' placeholder={"9 digits"}/>
                    </div>

                    <div className={'editInput'}>
                        <label >app number: </label>
                        <input type = 'text' id = 'app_number' placeholder={"the number of app"}/>
                    </div>

                    <div className={'editInput'}>
                        <label >type: </label>
                        <input type = 'text' id = 'type' placeholder={"e.g. \"primary\""}/>
                    </div>

                    <div className={'editInput'}>
                        <label >expiry date: </label>
                        <input type = 'date' id = 'expiry_date'/>
                    </div>

                </div>
            </div>
            <button onClick={editInfo} >
                create
            </button>
            <p className={'editInput'}>
                {createState}
            </p>


        </div>

    )
}
export default CreateWWCC;
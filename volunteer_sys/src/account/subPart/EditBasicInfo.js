import React, {useState} from "react";
import "../Account.css"
import Axios from "axios";

const EditBasicInfo = (props) =>{
    console.log(1);
    Axios.defaults.withCredentials = true;
    const [editState, setEditState] = useState("");
    const editInfo = () => {
        let preferredName = document.getElementById('preferredName').value;
        let email = document.getElementById('email').value;
        let phoneNumber = document.getElementById('phoneNumber').value;
        let birth = document.getElementById('birth').value;
        let ECRName = document.getElementById('ECRName').value;
        let wwccNumber = document.getElementById('wwccNumber').value;
        let familyRole = document.getElementById('familyRole').value;
        let ECRPhoneNumber = document.getElementById('ECRPhoneNumber').value;
        let roleId = "{"+document.getElementById('roleId').value+"}";

        Axios.post('http://localhost:3003/editEmergencyContactRole', {
            ECRName: ECRName,
            familyRole: familyRole,
            ECRPhoneNumber: ECRPhoneNumber,

        }).then((response)=>{
            console.log(response);
            setEditState("edit successfully")
        }, (error) => {
            console.log(error);
            setEditState("edit failed")

        },[]);
        Axios.post('http://localhost:3003/userBasicInfoEdit', {
            // userId: 6,
            userId: props.userId,
            preferredName: preferredName,
            email: email,
            phoneNumber: phoneNumber,
            birth: birth,
            roleId:roleId,

        }).then((response)=>{
            console.log(response);
            // setEditState("edit successfully")
        }, (error) => {
            console.log(error);
            setEditState("edit failed")


        },[]);

        Axios.post('http://localhost:3003/BasicInfoEdit', {
            // userId: 6,
            userId: props.userId,
            preferredName: preferredName,
            email: email,
            phoneNumber: phoneNumber,
            birth: birth,
            wwccNumber:wwccNumber,
            roleId:roleId,

        }).then((response)=>{
            if (response.data == "success to edit"){
                setEditState(response.data)
            }else{
                setEditState("Some information has not been filled in completely. Maybe WWCC number not exist, you can create it first. ")
            }
            console.log(response);
        }, (error) => {
            setEditState("edit failed")


        },[]);
    };

    return(
        <div >
            <div>
                <div>

                    <div className={'editInput'}>
                        <label >preferred name: </label>
                        <input type = 'text' id = 'preferredName' />
                    </div>

                    <div className={'editInput'}>
                        <label >Role Id: </label>
                        <input type = 'text' id = 'roleId' placeholder={"e.g. \"1,2,3\".Should always include 1."} size={40}/>
                    </div>

                    <div className={'editInput'}>
                        <label >phone number: </label>
                        <input type = 'text' id = 'phoneNumber' placeholder={"your phone number"}/>
                    </div>

                    <div className={'editInput'}>
                        <label >email: </label>
                        <input type = 'text' id = 'email' placeholder={"your email address"}/>
                    </div>

                    <div className={'editInput'}>
                        <label >Birth: </label>
                        <input type = 'date' id = 'birth' />
                    </div>

                    <div className={'editInput'}>
                        <label >WWCCNumber: </label>
                        <input type = 'text' id = 'wwccNumber' placeholder={"9 digits"}/>
                    </div>

                    <div className={'editInput'}>
                        <label >name of emergency cotact role: </label>
                        <input type = 'text' id = 'ECRName'/>
                    </div>

                    <div className={'editInput'}>
                        <label >family role: </label>
                        <input type = 'text' id = 'familyRole' placeholder={"e.g. \"mother\""}/>
                    </div>

                    <div className={'editInput'}>
                        <label >phone number of emergency contact role: </label>
                        <input type = 'text' id = 'ECRPhoneNumber' placeholder={"his/her phone number."}/>
                    </div>

                </div>
            </div>
            <button onClick={editInfo} >
                save
            </button>
            <p className={'editInput'}>
                {editState}
            </p>


        </div>

    )
}
export default EditBasicInfo;
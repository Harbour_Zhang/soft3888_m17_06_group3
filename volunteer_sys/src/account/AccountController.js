import React, {useEffect, useState} from "react";
import AccountBanner, {currentComponent} from "./AccountBanner";
import AccountMain from "./AccountMain";
import {useHistory} from "react-router-dom";
import UserCheck from "../security/UserCheck";

// Control all things happen in Account pages, make no url jump needed
export const AccountController = (props) =>{
    const [showComponent, setShowComponent] = useState(<AccountMain/>);
    const userId = props.history.location.state?.userId;
    function setComponent(){
        setShowComponent(currentComponent);
    }

    let history = useHistory();
    useEffect(() => {
        UserCheck().then(res => {
            if(!res){
                history.push('/login');
            }
        });
    },[])

    return (
        <div className= "App">
            <AccountBanner setComponent={setComponent} userId={userId}/>
            <div>{showComponent}</div>
        </div>
    );
}


export default AccountController;
import React from "react";
import {Redirect} from "react-router-dom";

function Dashboard({authorized}){
    if(!authorized){
        return <Redirect to={'/login'}/>;
    }
    return <p>Here is the dashboard</p>;
}

export default Dashboard;
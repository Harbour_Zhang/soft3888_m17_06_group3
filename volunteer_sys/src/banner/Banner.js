import React, { Component } from 'react';
import logo from '../image/logo.png';
import '../App.css';
import RoleBanner from "../banner/RoleBanner";

class Banner extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const role = this.props.role;
    return (

      <header role="banner">
          <div className="container">
              <div className="brand">
                  <h2>
                      <a href="/">
                          <img id='logo' src={logo} alt="Logo"/>
                      </a>
                  </h2>
              </div>
              <nav>
                <RoleBanner role={role}/>
              </nav>
              <nav role="navigation">
                  <ul>
                      <li>
                          <a href="">Module</a>
                      </li>
                      <li>
                          <a href="">History</a>
                      </li>

                      <li className="highlight">
                          <a href="">Account</a>
                      </li>
                  </ul>
              </nav>
          </div>
      </header>
    );
  }
}


export default Banner;

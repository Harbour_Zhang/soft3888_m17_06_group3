import React, { Component } from 'react';
import logo from '../image/logo.png';
import '../App.css';

class RoleBanner extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const role = this.props.role;
    if(role=='admin'){
      return (

            <div className="nav_wrapper">
                <ul>
                    <li>
                        <a href="/adminPeople">People</a>
                    </li>
                    <li>
                        <a href="/AdminRecord" onClick={() => this.handleClick}>Record</a>
                    </li>
                    <li>
                        <a href="/AdminOrder" onClick={() => this.handleClick}>Orders</a>
                    </li>
                    <li>
                        <a href="/AdminWWCC" onClick={() => this.handleClick}>WWCCs</a>
                    </li>
                    <li>
                        <a href="/AdminAnalytic" onClick={() => this.handleClick}>Analytics</a>
                    </li>
                </ul>
            </div>
          );
    }else if(role=='volunteer'){
      return (

        <div className="nav_wrapper">
            <ul>
                <li>
                    <a href="/volunteer/traningModule" onClick={() => this.handleClick}>Training</a>
                </li>
                <li>
                    <a href="/volunteer/WWCC" onClick={() => this.handleClick}>WWCCNumber</a>
                </li>
                <li>
                    <a href="/volunteer/history" onClick={() => this.handleClick}>AttendHistory</a>
                </li>
                <li>
                    <a href="/volunteerController" onClick={() => this.handleClick}>Portrals</a>
                </li>
            </ul>
        </div>
      );
    }else{
      return(<div className="nav_wrapper"><ul/><li/></div>);
    }
  }
}


export default RoleBanner;

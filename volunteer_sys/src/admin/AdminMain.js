import React, {useEffect, useState} from "react";

import '../App.css';
import './admin.css';
import NewsSlides from "../newsSlides/NewsSlides";

export const Admin = () => {
    const role = 'admin';
    return(
      <div className='background'>
        <h1 className={"title"}>Team Admin Portal</h1>
        {/*slides show component*/}
        <NewsSlides />
      </div>
    );
}

export default Admin;

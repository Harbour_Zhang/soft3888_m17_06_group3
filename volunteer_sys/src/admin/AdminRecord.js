import React, {useEffect, useState} from "react";
import '../App.css';
import './admin.css';
import RecordInfo from "./RecordInfo.js";
import NewRecord from "./NewRecord.js";
import Axios from "axios";
import moment from 'moment'
import UserCheck from "../security/UserCheck";
import {useHistory} from "react-router-dom";


require('dotenv').config();
const host = process.env.REACT_APP_HOST;
const port = process.env.REACT_APP_DB_NODE_PORT;
const path = 'http://' + host + ':' + port;

let currentComponent = <Record />;

function createData(recordid, legal_name, incidentid, street_name, birth_year, registered_date, blanket_number, bags_number) {
    return {
        recordid, legal_name, incidentid, street_name, birth_year, registered_date, blanket_number, bags_number
    };
}

function Record (props){
    return (
        <a onClick={() => {
            currentComponent = <RecordInfo recordid={props.recordid}
                                           legal_name={props.legal_name} incidentid={props.incidentid}
                                           street_name={props.street_name} birth_year={props.birth_year}
                                           registered_date={props.registered_date} blanket_number={props.blanket_number}
                                           bags_number={props.bags_number} />;
            props.setComponent();
        }}>
            <div className="person">
                <h1>{props.legal_name}</h1>
            </div>
        </a>
    );
}

export const AdminRecord = (props) => {
    let history = useHistory();

    const [showComponent, setShowComponent] = useState(<Record />);
    function setComponent(){
        setShowComponent(currentComponent);
    }

    Axios.defaults.withCredentials = true;

    const [result,setResult] = useState(<div>Looking up in the database, please wait a bit.</div>);
    useEffect(() => {
        UserCheck().then(res => {
            if(!res){
                history.push('/login');
            }
        });
        console.log(props.userId);
        Axios.get(path + '/admin/view_record').then((response) => {
            const allRecords = response.data.recordList.map((record) => createData(record.recordid,
                                record.legal_name, record.incidentid, record.street_name,
                                record.birth_year, moment(new Date(record.registered_date)).format("DD/MM/YYYY"),
                                record.blanket_number,
                                record.bags_number));
            setResult(
                <div className="sidenavA">
                    <h5>View Record</h5>
                    <div className="vertical-center">
                        <button className="center" onClick={() => {
                            currentComponent =<NewRecord userId={props.userId} />;
                            setComponent();
                        }}>Add new Record</button>
                    </div>
                    {allRecords.map((record) => (
                        <Record setComponent={setComponent} key={record.recordid} recordid={record.recordid}
                                legal_name={record.legal_name} incidentid={record.incidentid}
                                street_name={record.street_name} birth_year={record.birth_year}
                                registered_date={record.registered_date} blanket_number={record.blanket_number}
                                bags_number={record.bags_number}/>
                    ))}
                </div>
            )
        })
    },[])


    return(
        <div>
            {result}
            <div>{showComponent}</div>
        </div>
  );
}

export default AdminRecord;

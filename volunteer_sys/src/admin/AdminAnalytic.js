import React, {useEffect, useState} from "react";
import {
    useHistory
} from "react-router-dom";
import UserCheck from "../security/UserCheck";
import Axios from "axios";

require('dotenv').config();
const host = process.env.REACT_APP_HOST;
const port = process.env.REACT_APP_DB_NODE_PORT;
const path = 'http://' + host + ':' + port;

export const AdminAnalytic = () => {

    let history = useHistory();

    Axios.defaults.withCredentials = true;
    const [result,setResult] = useState(<div>Looking up in the database, please wait a bit.</div>);

    useEffect(() => {
        // UserCheck().then(res => {
        //     if(!res){
        //         history.push('/login');
        //     }
        // });
        Axios.get(path + '/admin/homelessCount').then((response) => {
            const count = response.data.counts.count;
            setResult(<div className="sidenavA">
                <h2>The total homeless client recorded in the system: {count}</h2>
            </div>
            )
        })
    },[])



    return(
        <div className="background">
          <h1>Analytic</h1>
          <div className="person">
              {result}
          </div>
        </div>
    );
}

export default AdminAnalytic;

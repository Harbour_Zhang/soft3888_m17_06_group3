import React, {useEffect, useState} from "react";
import Axios from "axios";
import PersonInfo from './PersonInfo.js';
import UserCheck from "../security/UserCheck";
import moment from 'moment'
import {useHistory} from "react-router-dom";

require('dotenv').config();
const host = process.env.REACT_APP_HOST;
const port = process.env.REACT_APP_DB_NODE_PORT;
const path = 'http://' + host + ':' + port;

function createData(full_name, userid, birth, preferred_name, roleid, phone_number, email, teamid) {
    return {
        full_name, userid, birth, preferred_name, roleid, phone_number, email, teamid
    };
}


let currentComponent = <PersonInfo user="none" />;

function Person (props){
    return (
        <a onClick={() => {
            currentComponent = <PersonInfo full_name={props.full_name}
                                           userid={props.userid} birth={props.birth} preferred_name={props.preferred_name}
                                           roleid={props.roleid} phone_number={props.phone_number} email={props.email}
                                           teamid={props.teamid} />;
            props.setComponent();
        }}>
        <div className="person">
            <h1>{props.full_name}</h1>
        </div>
        </a>
    );
}

export const AdminPeople = () => {
    let history = useHistory();
    UserCheck().then(res => {
        if(!res){
            history.push('/login');
        }
    });
    const [showComponent, setShowComponent] = useState(<Person />);
    function setComponent(){
        setShowComponent(currentComponent);
    }

    Axios.defaults.withCredentials = true;

    const [result,setResult] = useState(<div>Looking up in the database, please wait a bit.</div>);
    useEffect(() => {
        UserCheck().then(res => {
            if(!res){
                history.push('/login');
            }
        });
        Axios.get(path + '/view_people').then((response) => {
            const allUsers = response.data.userList.map((user) => createData(user.full_name, user.userid,
                            moment(new Date(user.birth)).format("DD/MM/YYYY"), user.preferred_name, user.roleid,
                                                        user.phone_number, user.email, user.teamid));
            setResult(<div className="sidenavA">
                {allUsers.map((user) => (
                    <Person setComponent={setComponent} key={user.userid} full_name={user.full_name}
                            userid={user.userid} birth={user.birth} preferred_name={user.preferred_name}
                            roleid={user.roleid} phone_number={user.phone_number} email={user.email} teamid={user.teamid}/>
                ))}
            </div>
            )
        })
    },[])

    return(
        <div>
            {result}
            <div>{showComponent}</div>
        </div>
    );
}

export default AdminPeople;

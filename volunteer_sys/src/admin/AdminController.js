import React, {useEffect, useState} from "react";
import AdminBanner, {currentComponent} from "./AdminBanner";
import AdminMain from "./AdminMain";
import {useHistory} from "react-router-dom";
import UserCheck from "../security/UserCheck";

// Control all things happen in TeamCoordinator pages, make no url jump needed
export const AdminController = (props) =>{
    const [showComponent, setShowComponent] = useState(<AdminMain/>);
    const userId = props.history.location.state?.userId;
    const roleId = props.history.location.state?.roleId;
    const currentRoleId = props.location.state?.currentRoleId;
    function setComponent(){
        setShowComponent(currentComponent);
    }

    // let history = useHistory();
    // useEffect(() => {
    //     UserCheck().then(res => {
    //         if(!res){
    //             history.push('/login');
    //         }
    //     });
    // },[])

    return (
        <div className= "App">
            <AdminBanner setComponent={setComponent} userId={userId} roleId={roleId} currentRole={currentRoleId}/>
            <div>{showComponent}</div>
        </div>
    );
}


export default AdminController;

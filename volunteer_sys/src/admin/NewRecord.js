import React, {useState} from "react";
import {TextField} from "@mui/material";
import Axios from "axios";

require('dotenv').config();
const host = process.env.REACT_APP_HOST;
const port = process.env.REACT_APP_DB_NODE_PORT;
const path = 'http://' + host + ':' + port;

const NewRecord = (props) =>{

    const [legalName, setLegalName] = useState('');
    const [street_name, setStreetName] = useState('');
    const [birth_year, setBirthYear] = useState('');
    const [registered_date, setRegistered_date] = useState(new Date());

    const addRecord = () => {
        console.log("adding");
        console.log(props.userId);
        setRegistered_date(new Date());
        Axios.post(path + '/adminCoordinator/addNewRecord', {
            legal_name: legalName, streetname: street_name, birth_year: birth_year, registered_date: registered_date, adminId: props.userId,
        }).then((response) => {
        })
        alert("Added successful!");
    }

    return (
        <div className="contentBack">
            <div className="PersonalInfo">
                <table>
                    <tbody>
                    <tr>
                        <th> Legal Name : </th>
                        <td> <TextField style={{backgroundColor:'#fff'}} className={'user-info-input'} id="filled-basic" label="LegalName" variant="filled" onChange={(e) => {
                            setLegalName(e.target.value);
                        }} /> </td>
                    </tr>
                    <tr>
                        <th> Street Name : </th>
                        <td> <TextField style={{backgroundColor:'#fff'}} className={'user-info-input'} id="filled-basic" label="StreetName" variant="filled" onChange={(e) => {
                            setStreetName(e.target.value);
                        }} /> </td>
                    </tr>
                    <tr>
                        <th> Year of Birth :</th>
                        <td> <TextField style={{backgroundColor:'#fff'}} className={'user-info-input'} id="filled-basic" label="BirthYear" variant="filled" onChange={(e) => {
                            setBirthYear(e.target.value);
                        }} /> </td>
                    </tr>
                    <tr>
                        <th> </th>
                        <td>
                            <button onClick={addRecord}>Add</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    );

}

export default NewRecord;

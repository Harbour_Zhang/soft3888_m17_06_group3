import React, {useEffect, useState} from "react";
import {
    useHistory
} from "react-router-dom";
import Axios from "axios";
import moment from 'moment'
import OrderInfo from './OrderInfo.js';
import UserCheck from "../security/UserCheck";

require('dotenv').config();
const host = process.env.REACT_APP_HOST;
const port = process.env.REACT_APP_DB_NODE_PORT;
const path = 'http://' + host + ':' + port;

let currentComponent = <OrderInfo text="none" />;

function createData(orderid, status, userid, homeless_record_id, blanket_number, bags_number, ordered_date, received_date, cloth, special_request, comments) {
    return {
        orderid, status, userid, homeless_record_id, blanket_number, bags_number, ordered_date, received_date, cloth, special_request, comments
    };
}

function Order (props){
    return (
        <a onClick={() => {
            currentComponent = <OrderInfo orderid={props.orderid} status={props.status} userid={props.userid}
                                          homeless_record_id={props.homeless_record_id} blanket_number={props.blanket_number}
                                          bags_number={props.bags_number} ordered_date={props.ordered_date} received_date={props.received_date}
                                          cloth={props.cloth} special_request={props.special_request} comments={props.comments} />;
            props.setComponent();
        }}>
            <div className="person">
                <h1>{props.orderid}</h1>
            </div>
        </a>
    );
}

export const AdminOrder = () => {

    let history = useHistory();

    const [showComponent, setShowComponent] = useState(<Order />);
    function setComponent(){
        setShowComponent(currentComponent);
    }

    Axios.defaults.withCredentials = true;

    const [resultNew,setResultNew] = useState(<div>Looking up in the database, please wait a bit.</div>);
    useEffect(() => {
        UserCheck().then(res => {
            if(!res){
                history.push('/login');
            }
        });
        Axios.post(path + '/view_order_submitted', {
        }).then((response) => {
            const allOrders = response.data.orderList.map((order) => createData(order.orderid, order.status,
                                                    order.userid, order.homeless_record_id, order.blanket_number,
                                                    order.bags_number, moment(new Date(order.ordered_date)).format("DD/MM/YYYY"), moment(new Date(order.received_date)).format("DD/MM/YYYY"),
                                                    order.cloth, order.special_request, order.comments));
            console.log(allOrders);
            console.log(response.data.orderList);
            setResultNew(<div>
                {allOrders.map((order) => (
                        <Order setComponent={setComponent} key={order.orderid} orderid={order.orderid} status={order.status} userid={order.userid}
                               homeless_record_id={order.homeless_record_id} blanket_number={order.blanket_number}
                               bags_number={order.bags_number} ordered_date={order.ordered_date} received_date={order.received_date}
                               cloth={order.cloth} special_request={order.special_request} comments={order.comments} />
                ))}
                </div>
            )
        })
    },[])

    const [resultProcessed, setResultProcessed] = useState(<div>Looking up in the database, please wait a bit.</div>);
    useEffect(() => {
        // console.log(path);
        Axios.post(path + '/view_order_processing', {
        }).then((response) => {
            const allOrders = response.data.orderList.map((order) => createData(order.orderid, order.status,
                order.userid, order.homeless_record_id, order.blanket_number,
                order.bags_number,  moment(new Date(order.ordered_date)).format("DD/MM/YYYY"), moment(new Date(order.received_date)).format("DD/MM/YYYY"),
                order.cloth, order.special_request, order.comments));
            setResultProcessed(<div>
                    {allOrders.map((order) => (
                        <Order setComponent={setComponent} key={order.orderid} orderid={order.orderid} status={order.status} userid={order.userid}
                               homeless_record_id={order.homeless_record_id} blanket_number={order.blanket_number}
                               bags_number={order.bags_number} ordered_date={order.ordered_date} received_date={order.received_date}
                               cloth={order.cloth} special_request={order.special_request} comments={order.comments} />
                    ))}
                </div>
            )
        })
    },[])

    const [resultCollected, setResultCollected] = useState(<div>Looking up in the database, please wait a bit.</div>);
    useEffect(() => {
        Axios.post(path + '/view_order_approved', {
        }).then((response) => {
            const allOrders = response.data.orderList.map((order) => createData(order.orderid, order.status,
                order.userid, order.homeless_record_id, order.blanket_number,
                order.bags_number,  moment(new Date(order.ordered_date)).format("DD/MM/YYYY"), moment(new Date(order.received_date)).format("DD/MM/YYYY"),
                order.cloth, order.special_request, order.comments));
            setResultCollected(<div>
                    {allOrders.map((order) => (
                        <Order setComponent={setComponent} key={order.orderid} orderid={order.orderid} status={order.status} userid={order.userid}
                               homeless_record_id={order.homeless_record_id} blanket_number={order.blanket_number}
                               bags_number={order.bags_number} ordered_date={order.ordered_date} received_date={order.received_date}
                               cloth={order.cloth} special_request={order.special_request} comments={order.comments} />
                    ))}
                </div>
            )
        })
    },[])

    const [resultRejected, setResultRejected] = useState(<div>Looking up in the database, please wait a bit.</div>);
    useEffect(() => {
        Axios.post(path + '/view_order_rejected', {
        }).then((response) => {
            const allOrders = response.data.orderList.map((order) => createData(order.orderid, order.status,
                order.userid, order.homeless_record_id, order.blanket_number,
                order.bags_number,  moment(new Date(order.ordered_date)).format("DD/MM/YYYY"), moment(new Date(order.received_date)).format("DD/MM/YYYY"),
                order.cloth, order.special_request, order.comments));
            setResultRejected(<div>
                    {allOrders.map((order) => (
                        <Order setComponent={setComponent} key={order.orderid} orderid={order.orderid} status={order.status} userid={order.userid}
                               homeless_record_id={order.homeless_record_id} blanket_number={order.blanket_number}
                               bags_number={order.bags_number} ordered_date={order.ordered_date} received_date={order.received_date}
                               cloth={order.cloth} special_request={order.special_request} comments={order.comments} />
                    ))}
                </div>
            )
        })
    },[])

    return(
        <div className="background" style={{'overflow-y':'scroll'}}>
            <div className="sidenavA">
                <h3> Submitted order</h3>
                {resultNew}
                <h3> Processing order</h3>
                {resultProcessed}
                <h3> Allowed order</h3>
                {resultCollected}
                <h3> Rejected order</h3>
                {resultRejected}
            </div>
            <div>{showComponent}</div>
        </div>
    );
}

export default AdminOrder;

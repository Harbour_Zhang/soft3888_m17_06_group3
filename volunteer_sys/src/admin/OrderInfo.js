import React from "react";

const OrderInfo = (props) =>{

    return (
        <div className="contentBack">
            <div className="PersonalInfo">
                <table>
                    <tbody>
                    <tr>
                        <th> Order ID: </th>
                        <td> {props.orderid} </td>
                    </tr>
                    <tr>
                        <th> Status: </th>
                        <td> {props.status}</td>
                    </tr>
                    <tr>
                        <th> User: </th>
                        <td> {props.userid}</td>
                    </tr>
                    <tr>
                        <th> Record: </th>
                        <td> {props.homeless_record_id}</td>
                    </tr>
                    <tr>
                        <th> Blanket :</th>
                        <td> {props.blanket_number}</td>
                    </tr>
                    <tr>
                        <th> Bag :</th>
                        <td> {props.bags_number}</td>
                    </tr>
                    <tr>
                        <th> Ordered date :</th>
                        <td> {props.ordered_date}</td>
                    </tr>
                    <tr>
                        <th> Recieved date:</th>
                        <td> {props.received_date}</td>
                    </tr>
                    <tr>
                        <th> Cloth :</th>
                        <td> {props.cloth}</td>
                    </tr>
                    <tr>
                        <th> Special Request :</th>
                        <td> {props.special_request}</td>
                    </tr>
                    <tr>
                        <th> Comments :</th>
                        <td> {props.comments}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    );

}
export default OrderInfo;

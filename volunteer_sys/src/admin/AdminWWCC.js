import React, {useEffect, useState} from "react";
import Axios from "axios";
import {useHistory} from "react-router-dom";
import moment from "moment";
import UserCheck from "../security/UserCheck";


require('dotenv').config();
const host = process.env.REACT_APP_HOST;
const port = process.env.REACT_APP_DB_NODE_PORT;
const path = 'http://' + host + ':' + port;

function createData(number, app_number, type, expiry_date, valid_status, userid, full_name) {
    return {
        number, app_number, type, expiry_date, valid_status, userid, full_name
    };
}

export const WWCC = () => {
    let history = useHistory();
    Axios.defaults.withCredentials = true;

    const [result,setResult] = useState(<div>Looking up in the database, please wait a bit.</div>);
    useEffect(() => {
        UserCheck().then(res => {
            if(!res){
                history.push('/login');
            }
        });
        Axios.get(path + '/admin/wwccNumber').then((response) => {
            // console.log("frontend: " + response.data);
            console.log(response.data);
            const allWWCC = response.data.wwccList.map((wwcc) => createData(wwcc.number, wwcc.app_number,
                        wwcc.type, moment(new Date(wwcc.expiry_date)).format("DD/MM/YYYY"), wwcc.valid_status, wwcc.userid, wwcc.full_name));

            setResult(
                <div className="center">
                    <table className='people'>
                        <tbody>
                            <tr>
                                <th colSpan="5"><h1>View WWCC</h1></th>
                            </tr>
                            <tr>
                                <th>User id</th>
                                <th>Name</th>
                                <th>WWCC number</th>
                                <th>App number</th>
                                <th>Type</th>
                                <th>Expire date</th>
                                <th>Valid Status</th>
                                <th></th>
                            </tr>
                            {allWWCC.map((wwcc) => (
                                <tr key={wwcc.userid}>
                                    <td>{wwcc.userid}</td>
                                    <td>{wwcc.full_name}</td>
                                    <td>{wwcc.number}</td>
                                    <td>{wwcc.app_number}</td>
                                    <td>{wwcc.type}</td>
                                    <td>{wwcc.expiry_date}</td>
                                    <td>{wwcc.valid_status}</td>
                                    <td><button onClick={() => {updateWWCC(wwcc.number)}}>confirm</button></td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            )
        })
    },[])

    function updateWWCC(wwccNumber) {
        Axios.post(path + '/admin/confirmWWCCStatus', {
            wwccNumber: wwccNumber,
        }).then((response) => {
        })
        alert("Update successful!");
        Axios.get(path + '/admin/wwccNumber').then((response) => {
            // console.log("frontend: " + response.data);
            console.log(response.data);
            const allWWCC = response.data.wwccList.map((wwcc) => createData(wwcc.number, wwcc.app_number,
                wwcc.type, moment(new Date(wwcc.expiry_date)).format("DD/MM/YYYY"), wwcc.valid_status, wwcc.userid, wwcc.full_name));

            setResult(
                <div className="center">
                    <table className='people'>
                        <tbody>
                        <tr>
                            <th colSpan="5"><h1>View WWCC</h1></th>
                        </tr>
                        <tr>
                            <th>User id</th>
                            <th>Name</th>
                            <th>WWCC number</th>
                            <th>App number</th>
                            <th>Type</th>
                            <th>Expire date</th>
                            <th>Valid Status</th>
                            <th></th>
                        </tr>
                        {allWWCC.map((wwcc) => (
                            <tr key={wwcc.userid}>
                                <td>{wwcc.userid}</td>
                                <td>{wwcc.full_name}</td>
                                <td>{wwcc.number}</td>
                                <td>{wwcc.app_number}</td>
                                <td>{wwcc.type}</td>
                                <td>{wwcc.expiry_date}</td>
                                <td>{wwcc.valid_status}</td>
                                <td><button onClick={() => {updateWWCC(wwcc.number)}}>confirm</button></td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
            )
        })
    }


    return(
        <div className="background">
          {result}
        </div>
    );
}

export default WWCC;

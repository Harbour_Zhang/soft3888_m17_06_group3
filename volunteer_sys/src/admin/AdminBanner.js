import React, {Component, useContext} from 'react';
import logo from '../image/logo.png';
import '../App.css';
import AdminController from "./AdminController";
import AdminMain from "./AdminMain";
import AdminOrder from "./AdminOrder";
import AdminPeople from "./AdminPeople";
import AdminRecord from "./AdminRecord";
import AdminWWCC from "./AdminWWCC";
import AdminAnalytic from "./AdminAnalytic";
import AccountMain from "../account/AccountMain";
import {FormControl, InputLabel, MenuItem, Select} from "@mui/material";
import {useHistory} from "react-router-dom";

export let currentComponent = <AdminMain />;

// Banner component, control all banner elements
export const AdminBanner = (props) => {
    let history = useHistory();
        return (
            <div>
                <header role="banner">
                    <div className="container">
                        <div className="brand">
                            <h2>
                                <a href="/">
                                    <img id='logo' src={logo} alt="Logo"/>
                                </a>
                            </h2>
                        </div>

                        <nav>
                            <div className="nav_wrapper">
                                <ul>
                                    {/*All top components*/}
                                    <li>
                                        <a onClick={() => {
                                            currentComponent = <AdminPeople />;
                                            props.setComponent();
                                        }}>People</a>
                                    </li>
                                    <li>
                                        <a onClick={() => {
                                            currentComponent = <AdminRecord userId={props.userId} />;
                                            props.setComponent();
                                        }}>Record</a>
                                    </li>
                                    <li>
                                        <a onClick={() => {
                                            currentComponent = <AdminOrder />;
                                            props.setComponent();
                                        }}>Orders</a>
                                    </li>
                                    <li>
                                        <a onClick={() => {
                                            currentComponent = <AdminWWCC />;
                                            props.setComponent();
                                        }}>WWCCs</a>
                                    </li>
                                    <li>
                                        <a onClick={() => {
                                            currentComponent = <AdminAnalytic />;
                                            props.setComponent();
                                        }}>Analytics</a>
                                    </li>
                                    <li>
                                        <a onClick={() => {
                                            currentComponent = <AdminMain />;
                                            props.setComponent();
                                        }}>Portal</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        {/*Navigation bar*/}
                        <nav role="navigation">
                            <ul>
                                <li>
                                    <a href="">Module</a>
                                </li>
                                <li>
                                    <a href="">History</a>
                                </li>

                                <li className="highlight">
                                    <a onClick={() => {
                                        currentComponent = <AccountMain userId={props.userId}/>;
                                        props.setComponent();
                                    }}>Account</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </header>
                <br />
                <div>
                    <FormControl style={{marginLeft:'80%'}}>
                        <InputLabel id="TeamSelectLabel">Roles</InputLabel>
                        <Select
                            required
                            labelId="role-select-label"
                            id="role-select"
                            value={props.currentRole}
                            label="Roles ID"
                            onChange={(e) => {
                                if(e.target.value === 1){
                                    history.push("/volunteerController", {userId: props.userId, roleId:props.roleId,  currentRoleId:e.target.value});
                                }else if(e.target.value === 2){
                                    history.push("/admin", {userId: 1, roleId:2, currentRoleId:2});
                                }else if(e.target.value === 3){
                                    history.push("/teamCoordinatorController", {userId: props.userId, roleId:props.roleId, currentRoleId:e.target.value});
                                }else if(e.target.value === 4){
                                    history.push("/teamLeaderController", {userId: props.userId, roleId:props.roleId,  currentRoleId:e.target.value});
                                }
                            }}
                        >
                            {props.roleId.map((eachId) => <MenuItem value={eachId}>{eachId}</MenuItem>)}
                        </Select>
                    </FormControl>
                </div>
            </div>
        );
}

export default AdminBanner;

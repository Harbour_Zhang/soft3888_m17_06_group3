import React, {useEffect, useState} from "react";

const PersonInfo = (props) =>{
    return (
      <div className="contentBack">
          <div className="PersonalInfo">
              <table>
                  <tbody>
                  <tr>
                      <th> User Name : </th>
                      <td> {props.full_name} </td>
                  </tr>
                  <tr>
                      <th> User Id : </th>
                      <td> {props.userid} </td>
                  </tr>
                  <tr>
                      <th> Birth : </th>
                      <td> {props.birth}</td>
                  </tr>
                  <tr>
                      <th> Preferredname :</th>
                      <td> {props.preferred_name}</td>
                  </tr>
                  <tr>
                      <th> Role_ID :</th>
                      <td> {props.roleid} </td>
                  </tr>
                  <tr>
                      <th> PhoneNumber :</th>
                      <td> {props.phone_number}</td>
                  </tr>
                  <tr>
                      <th> Email :</th>
                      <td> {props.email} </td>
                  </tr>
                  <tr>
                      <th> Team_ID :</th>
                      <td>  {props.teamid}</td>
                  </tr>
                  </tbody>
              </table>
          </div>
      </div>
    );
}

export default PersonInfo;

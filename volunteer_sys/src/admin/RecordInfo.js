import React from "react";

const RecordInfo = (props) =>{

    // const [incidentId,setIncidentId] = useState('');
    // const incidentId = props.incidentid.map((id) => {
    //     <p>{id}</p>
    // };

    // const incidentId = props.incidentid.map((number) =>
    //     <p key={number.toString()}>
    //         {number}
    //     </p>
    // );

    return (
        <div className="contentBack">
            <div className="PersonalInfo">
                <table>
                    <tbody>
                    <tr>
                        <th> Legal Name : </th>
                        <td> {props.legal_name} </td>
                    </tr>
                    <tr>
                        <th> Record ID : </th>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <th> Incident ID : </th>
                        <td> {props.incidentid}</td>
                    </tr>
                    <tr>
                        <th> Street Name : </th>
                        <td> {props.street_name}</td>
                    </tr>
                    <tr>
                        <th> Year of Birth :</th>
                        <td> {props.birth_year}</td>
                    </tr>
                    <tr>
                        <th> Registered :</th>
                        <td> {props.registered_date}</td>
                    </tr>
                    <tr>
                        <th> Ordered Blankets :</th>
                        <td> {props.blanket_number} </td>
                    </tr>
                    <tr>
                        <th> Ordered Bags :</th>
                        <td> {props.bags_number} </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    );

}

export default RecordInfo;

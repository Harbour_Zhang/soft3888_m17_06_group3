
import React, {useEffect, useState} from "react";
import VolunteerBanner, {currentComponent} from "./VolunteerBanner";
import './Volunteer.css';
import VolunteerMain from "./VolunteerMain";
import {useHistory} from "react-router-dom";
import UserCheck from "../security/UserCheck";

// Control all things happen in TeamCoordinator pages, make no url jump needed
export const VolunteerController = (props) =>{
    let history = useHistory();
    const [showComponent, setShowComponent] = useState(<VolunteerMain/>);
    const userId = props.history.location.state?.userId;
    const roleId = props.history.location.state?.roleId;
    const currentRoleId = props.location.state?.currentRoleId;

    function setComponent(){
        setShowComponent(currentComponent);
    }

    useEffect(() => {
        UserCheck().then(res => {
            if(!res){
                history.push('/login');
            }
        });
    },[])

    return (
        <div className= "App">
            <VolunteerBanner setComponent={setComponent} userId={userId} roleId={roleId} currentRole={currentRoleId}/>
            <div>{showComponent}</div>
        </div>
    );
}


export default VolunteerController;
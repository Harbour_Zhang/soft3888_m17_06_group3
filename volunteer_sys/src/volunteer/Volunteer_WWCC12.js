/*import React, {useEffect, useState} from "react";
import Axios from "axios";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import IconButton from "@mui/material/IconButton";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import Collapse from "@mui/material/Collapse";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import Paper from "@mui/material/Paper";
import {TablePagination} from "@mui/material";
import Banner from './VolunteerBanner.js';
import {useHistory} from "react-router-dom";
import UserCheck from "../security/UserCheck";



function createData(number, APP_number, type , expiry_date, valid_status) {
    return {
        number, APP_number, type , expiry_date, valid_status,
        Details: [
            {
                number:number,
                APP_number:APP_number,
                type:type,
                expiry_date:expiry_date,
                valid_status:valid_status
            }
        ]
    };
}

function Row(props) {
    const { row } = props;
    const [open, setOpen] = React.useState(false);

    return (
        <React.Fragment>
            <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell >
                </TableCell>
                <TableCell component="th" scope="row">
                    {row.number}
                </TableCell>
                <TableCell>{row.APP_number}</TableCell>
                <TableCell>{row.type}</TableCell>
                <TableCell>{row.expiry_date}</TableCell>
                <TableCell>{row.valid_status}</TableCell>
            </TableRow>
            
        </React.Fragment>
    );
}

const Volunteer_WWCC  = (props) =>{
    const role = 'volunteer';
    const [result,setResult] = useState(<div>Looking up in the database, please wait a bit.</div>);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    let history = useHistory();
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };


    

    useEffect(() => {
        UserCheck().then(res => {
            if(!res){
                history.push('/login');
            }
        });
        Axios.post('http://localhost:3003/volunteer/wwccNumber',{
            volunteerId: props.userId,
        }).then((response) => {
            console.log(response);
            const allRows = response.data.allRows.map((eachRow) => createData(eachRow.number, eachRow.app_number,eachRow.type, eachRow.expiry_date, eachRow.valid_status));
            setResult(
                <div style={{'marginLeft':'10%', marginRight:'10%'}}>
                    <Paper sx={{ width: '100%', overflow: 'hidden' }}>
                        <TableContainer sx={{ maxHeight: 800 }}>
                            <Table stickyHeader aria-label="sticky table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell />
                                        <TableCell>Number</TableCell>
                                        <TableCell>App number</TableCell>
                                        <TableCell>Type</TableCell>
                                        <TableCell>Expiry Date</TableCell>
                                        <TableCell>Valid Status</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {allRows
                                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                        .map((row) => {
                                            return (
                                                <Row key={row.number} row={row} />
                                            );
                                        })}
                                </TableBody>
                            </Table>
                        </TableContainer>
                        <TablePagination
                            rowsPerPageOptions={[5,10, 25, 50]}
                            component="div"
                            count={allRows.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                        />
                    </Paper>
                </div>
            )
        })
    },[])
    return (
        <div>
            {/*<Banner role={role}/>*///}
            /*<h1 className={"title"}>WWCC Number</h1>
            
            {result}
        </div>
    );
};

export default Volunteer_WWCC;*/
import React, {useEffect, useState} from "react";

import logo from '../image/logo.png';
import VolunteerBanner from './VolunteerBanner.js';
import '../App.css';
import Axios from "axios";


const Volunteer_traningModule = (props) =>{
  function sendInfo(){
      Axios.post('http://localhost:3003/volunteer/finishedTraining', {
          volunteerId : props.userId
      }).then((response)=>{
          console.log(response.data);
      },(error) => {
          console.log(error);
      })
  }
  const [status, setStatus] = useState();

  useEffect(()=>{
      Axios.post('http://localhost:3003/volunteer', {
          userId : props.userId
      }).then((response) =>{
          console.log(response);
          console.log(response.data);
          if (response.data.trainingModel === 0){
              setStatus('unfinished');
          }else{
              setStatus('Finished');
          }

      },(error) => {
          console.log(error);
      })
  }, [status]);

  return (

      <div>
          <p >Your Training Module Status: {status} </p>
          <br/>

          <a  href='https://www.volunteeringaustralia.org/wp-content/files_mf/1377052716VaGuidetotrainingvolunteerspartA.pdf' onClick={sendInfo}  >Click Here to Complete Training Module</a>

      </div>


  )
}

export default Volunteer_traningModule;
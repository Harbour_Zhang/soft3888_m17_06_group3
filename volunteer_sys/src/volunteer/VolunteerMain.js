import React from "react";
import './Volunteer.css';
import NewsSlides from "../newsSlides/NewsSlides";

//The main page component
const VolunteerMain = () =>{
    return (
        <div className= "App">
                <h1 className={"title"}> Volunteer Portal</h1>
                {/*slides show component*/}
                <NewsSlides />
        </div>
    );
}


export default VolunteerMain;
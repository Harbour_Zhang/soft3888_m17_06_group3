import React, {useEffect, useState} from "react";
import Axios from "axios";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import IconButton from "@mui/material/IconButton";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import Collapse from "@mui/material/Collapse";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import Paper from "@mui/material/Paper";
import {TablePagination} from "@mui/material";
import Banner from './VolunteerBanner.js';
import {useHistory} from "react-router-dom";
import UserCheck from "../security/UserCheck";



function createData(team_name, team_night, vehicle_condition_report , start_time, end_time) {
    return {
        team_name, team_night, vehicle_condition_report , start_time, end_time,
        Details: [
            {
                team_name:team_name,
                team_night:team_night,
                vehicle_condition_report:vehicle_condition_report,
                start_time:start_time,
                end_time:end_time
            }
        ]
    };
}

function Row(props) {
    const { row } = props;
    const [open, setOpen] = React.useState(false);

    return (
        <React.Fragment>
            <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell >
                </TableCell>
                <TableCell component="th" scope="row">
                    {row.team_name}
                </TableCell>
                <TableCell>{row.team_night}</TableCell>
                <TableCell>{row.vehicle_condition_report}</TableCell>
                <TableCell>{row.start_time}</TableCell>
                <TableCell>{row.end_time}</TableCell>
            </TableRow>
            
        </React.Fragment>
    );
}

const Volunteer_history  = (props) =>{
    const role = 'volunteer';
    const [result,setResult] = useState(<div>Looking up in the database, please wait a bit.</div>);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    let history = useHistory();
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    useEffect(() => {
        UserCheck().then(res => {
            if(!res){
                history.push('/login');
            }
        });
        Axios.post('http://localhost:3003/volunteer/attendHistory',{
            volunteerId: props.userId,
        }).then((response) => {
            const allRows = response.data.allRows.map((eachRow) => createData(eachRow.team_name, eachRow.team_night,eachRow.vehicle_condition_report, eachRow.start_time, eachRow.end_time));
            setResult(
                <div style={{'marginLeft':'10%', marginRight:'10%'}}>
                    <Paper sx={{ width: '100%', overflow: 'hidden' }}>
                        <TableContainer sx={{ maxHeight: 800 }}>
                            <Table stickyHeader aria-label="sticky table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell />
                                        <TableCell>Team Name</TableCell>
                                        <TableCell>Team Night</TableCell>
                                        <TableCell>Vehicle_condition_report</TableCell>
                                        <TableCell>Start Time</TableCell>
                                        <TableCell>End Time</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {allRows
                                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                        .map((row) => {
                                            return (
                                                <Row key={row.team_name} row={row} />
                                            );
                                        })}
                                </TableBody>
                            </Table>
                        </TableContainer>
                        <TablePagination
                            rowsPerPageOptions={[5,10, 25, 50]}
                            component="div"
                            count={allRows.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                        />
                    </Paper>
                </div>
            )
        })
    },[])
    return (
        <div>
            {/*<Banner role={role}/>*/}
            <h1 className={"title"}>Attend History</h1>
            
            {result}
        </div>
    );
};

export default Volunteer_history ;
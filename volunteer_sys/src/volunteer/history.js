import React, { Component } from 'react';
import './history.css';

function History (props){
    return (
        <a href={props.html}>
            <div className = "wwcc" style={{'border':`3px solid #004a99`, 'background-color':`#FEDE00`}}>
                <h1>{props.text}</h1>
            </div>
        </a>
    );

}


function HistoryInfo (props){

    return (
        <div>
            <div style={{'display':`plex`}}>
                <div style={{'font-size':`xx-large`}}> Team_name : {props.team_name}</div>
                <div style={{'font-size':`xx-large`}}> Team_night : {props.team_night}</div>
                <div style={{'font-size':`xx-large`}}> Team_vehicle : {props.vehicle_condition_report}</div>
                <div style={{'font-size':`xx-large`}}> Start_time : {props.start_time}</div>
                <div style={{'font-size':`xx-large`}}> End_time : {props.end_time}</div>

            </div>
        </div>
    );
}


export {
    History,
    HistoryInfo,
} 
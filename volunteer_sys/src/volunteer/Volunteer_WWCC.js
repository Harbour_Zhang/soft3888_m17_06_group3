import React, {useEffect, useState}  from "react";
import {
    useHistory
} from "react-router-dom";

import Banner from './VolunteerBanner.js';
import logo from '../image/logo.png';
import Axios from "axios";
import {WWCC, WWCCInfo} from './WWCC.js';
import UserCheck from "../security/UserCheck";


export const Volunteer_WWCC = (props) => {

    const role = 'volunteer';
    let history = useHistory();
    
    Axios.defaults.withCredentials = true;

    const [Info,setWwccList] = useState(<div>Looking up in the database, please wait a bit.</div>);
    useEffect(() => {
        UserCheck().then(res => {
            if(!res){
                history.push('/login');
            }
        });
        Axios.post('http://localhost:3003/volunteer/wwccNumber',{
            volunteerId: props.userId,
        }).then((response) => {
            console.log(response);
            setWwccList({
                           number:response.data.number,
                           APP_number:response.data.APP_number,
                           type:response.data.type,
                           expiry_date:response.data.expiry_date,
                           valid_status:response.data.valid_status
                         });
            console.log(Info);
        })
    },[])   
    return(
        
        <div>
            
            <div className = "App">
            <div className = "personalInfoBox">
            {/*<Banner role={role}/>*/}
                <p> WWCCInformation</p>
                <div className="sidenavA">
                
            <WWCCInfo number = {Info.number}  app_number = {Info.APP_number} type = {Info.type} expiry_date = {Info.expiry_date} valid_status = {Info.valid_status}/>
                    
                </div>
            </div>
        </div>
    </div>
    );
}

















export default Volunteer_WWCC;

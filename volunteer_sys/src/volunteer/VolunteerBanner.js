import logo from '../image/logo.png';
import '../App.css';
import {FormControl, InputLabel, MenuItem, Select} from "@mui/material";
import {useHistory} from "react-router-dom";
import AccountMain from "../account/AccountMain";
import Volunteer_WWCC from "./Volunteer_WWCC";
import Volunteer_history from "./volunteer_history";
import VolunteerController from "./VolunteerController";
import VolunteerMain from "./VolunteerMain";
import Volunteer_traningModule from './Volunteer_traningModule';
import React, {useEffect, useState} from "react";

import Axios from "axios";




export let currentComponent = <VolunteerMain />;
var training;
// Banner component, control all banner elements
export const VolunteerBanner = (props) => {
    let history = useHistory();
    

    const [status, setStatus] = useState();

  useEffect(()=>{
      Axios.post('http://localhost:3003/volunteer', {
          teamid : props.userId
      }).then((response) =>{
          if (response.data.status === 0){
              setStatus('unfinished');
          }else{
              setStatus('Finished');
          }

      },(error) => {
          console.log(error);
      })
  }, [status]);

   
    return (
        
        <div>
            <header role="banner">
                <div className="container">
                    <div className="brand">
                        <h2>
                            <a href="/">
                                <img id='logo' src={logo} alt="Logo"/>
                            </a>
                        </h2>
                    </div>

                    <nav>
                        <div className="nav_wrapper">
                            <ul>
                                <li>
                                </li>
                                <li>
                                </li>
                                <li>
                                </li>
                                <li>
                                    
                                    <a onClick={() => {
                                        currentComponent = <VolunteerMain />;
                                        props.setComponent();
                                    }}>Portal</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    {/*Navigation bar*/}
                    <nav role="navigation">
                        <ul>
                            <li>
                                
                                <a onClick={() => {
                                        currentComponent = <Volunteer_traningModule userId={props.userId}/>;
                                        props.setComponent();
                                    }}>Training</a>
                            </li>
                            <li>
                            
                                <a onClick={() => {
                                    if(status == 'Finished'){
                                        currentComponent = <Volunteer_WWCC userId={props.userId}/>;
                                    props.setComponent();
                                    }
                                    
                                }}>WWCCNumber</a>
                            
                                
                            </li>
                            <li>
                        
                                <a onClick={() => {
                                    if(status == 'Finished'){
                                        currentComponent = <Volunteer_history userId={props.userId}/>;
                                    props.setComponent();
                                    }
                                    
                                }}>History</a>
                                
                            
                            
                            </li>
                            <li className="highlight">
                                 <a onClick={() => {
                                    currentComponent = <AccountMain userId={props.userId}/>;
                                    props.setComponent();
                                }}>Account</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </header>
            <br />
            <div>
                <FormControl style={{marginLeft:'80%'}}>
                    <InputLabel id="TeamSelectLabel">Roles</InputLabel>
                    <Select
                        required
                        labelId="role-select-label"
                        id="role-select"
                        value={props.currentRole}
                        label="Roles ID"
                        onChange={(e) => {
                            if(e.target.value === 2){
                                history.push('/admin', {userId: props.userId, roleId:props.roleId,  currentRoleId:e.target.value});
                            }else if(e.target.value === 3){
                                history.push("/teamCoordinatorController", {userId: props.userId, roleId:props.roleId, currentRoleId:e.target.value});
                            }else if(e.target.value === 4){
                                history.push("/teamLeaderController", {userId: props.userId, roleId:props.roleId,  currentRoleId:e.target.value});
                            }
                        }}
                    >
                        {props.roleId.map((eachId) => <MenuItem value={eachId}>{eachId}</MenuItem>)}
                    </Select>
                </FormControl>
            </div>
        </div>
    );
}

export default VolunteerBanner;

import React, { Component } from 'react';
import logo from '../image/logo.png';
import '../App.css';

class LoginBanner extends React.Component {


    render() {
        return (
            <header role="banner">
                <div className="container">
                    <div className="brand">
                        <h2>
                            <a href="/">
                                <img id='logo' src={logo} alt="Logo"/>
                            </a>
                        </h2>
                    </div>
                </div>
            </header>
        );
    }
}


export default LoginBanner;

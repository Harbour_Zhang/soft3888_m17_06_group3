import React, {useEffect, useState} from "react";
import Banner from '../banner/Banner';
import './Login.css';
import {
    useHistory
} from "react-router-dom";
import Axios from "axios";
import LoginBanner from "./LoginBanner";
import {TextField} from "@mui/material";

export var loginStatusGlobal;

export const Login = () => {
    let history = useHistory();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [loginStatus, setLoginStatus] = useState(false);
    const [loginMessage, setLoginMessage] = useState('');
    
    Axios.defaults.withCredentials = true;

    let userIdLocal = 0;

    const userAuthed = () => {
        Axios.get('http://localhost:3003/userVerify', {
            headers: {
                "x-access-token": localStorage.getItem("token"),
            }
        }).then((response) => {
            if(response.data.auth){
                loginStatusGlobal = true;
            }else{
                loginStatusGlobal = false;
            }
        })
    }

    let roleID;
    const fetchUserRole = () => {
        Axios.post('http://localhost:3003/user', {
            userId: userIdLocal,
        }).then((response)=>{
            roleID = response.data.roleId;
            if(roleID[0] === 1){
                history.push("/volunteerController", {userId: userIdLocal, roleId:response.data.roleId, currentRoleId: roleID[0]});
            }else if(roleID[0] === 2){
                history.push("/admin", {userId: userIdLocal, roleId:response.data.roleId, currentRoleId: roleID[0]});
            }else if(roleID[0] === 3){
                history.push("/teamCoordinatorController", {userId: userIdLocal, roleId:response.data.roleId, currentRoleId: roleID[0]});
            }else if(roleID[0] === 4){
                history.push("/teamLeaderController", {userId: userIdLocal, roleId:response.data.roleId, currentRoleId: roleID[0]});
            }
        });
    }

    const loginToServer = () => {
        Axios.post('http://localhost:3003/login', {
            email: email,
            password: password,
        }).then((response) => {
            if(!response.data.auth){
                setLoginStatus(false);
                setLoginMessage(response.data.incorrect_message);
            }else{
                setLoginStatus(true);
                localStorage.setItem("token", response.data.token);
                userIdLocal = response.data.Id;
                userAuthed();
                fetchUserRole();
                setLoginMessage(response.data.result.rows[0].mail_address);
            }
        });
    }

    const redirectToRegister = () => {
        history.push("/register");
    }

    useEffect(() => {
        Axios.get('http://localhost:3003/login').then( (response) => {
            if(response.data.loggedIn === true){
                Axios.get('http://localhost:3003/userVerify', {
                    headers: {
                        "x-access-token": localStorage.getItem("token"),
                    }
                }).then((res) => {
                    if(res.data.auth){
                        loginStatusGlobal = true;
                        userIdLocal = response.data.user.rows[0].userid;
                        setLoginMessage(response.data.user.rows[0].mail_address);
                        setLoginStatus(true);
                        fetchUserRole();
                    }else{
                        loginStatusGlobal = false;
                    }
                })
            }
        })
    }, [])

    return (
        <div className={"App"}>
            <div className={"Login"}>
                <LoginBanner />
                <div className={"login-box"}>
                    <h1 className={"login-title"}>Volunteer Portal</h1>
                    <br/>
                    <TextField className={'user-info-input'} id="filled-basic" label="Email" variant="filled" onChange={(e) => {
                        setEmail(e.target.value);
                    }} />
                    <br/>
                    <TextField className={'user-info-input'} id="filled-basic" label="Password" variant="filled" type={'password'} onChange={(e) => {
                        setPassword(e.target.value);
                    }}/>
                    <br/>
                    <h1 className={"warnMessage"}>{loginMessage}</h1>
                    <br />
                    <button onClick={loginToServer} className={"btn-outline-primary"}>Login</button>
                    <h1 className={"registerText"} onClick={redirectToRegister}>Do not have an account? Click to register.</h1>
                </div>
            </div>
        </div>
    );
}


export default Login;

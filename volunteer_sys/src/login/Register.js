import React, {useState} from "react";
import Axios from 'axios';
import "./Login.css"
import LoginBanner from "./LoginBanner";
import {TextField} from "@mui/material";
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import DateAdapter from '@mui/lab/AdapterDateFns';
import {LocalizationProvider} from "@mui/lab";

const Register = () => {

    const [emailReg, setEmailReg] = useState('');
    const [passwordReg, setPasswordReg] = useState('');
    const [fullName, setFullName] = useState('');
    const [preferName, setPreferName] = useState('');
    const [phone, setPhone] = useState('');
    const [value, setValue] = React.useState(new Date('2014-08-18T21:11:54'));

    const handleChange = (newValue) => {
        setValue(newValue);
    };

    const [registerMessage, setRegisterMessage] = useState('');

    const register = () => {
        Axios.post('http://localhost:3003/register', {
            email: emailReg,
            password: passwordReg,
        }).then((response) => {
            setRegisterMessage(response.data.returnMessage);
        });
        Axios.post('http://localhost:3003/registerDetails', {
            email: emailReg,
            fullName:fullName,
            birth:value,
            preferName:preferName,
            phoneNumber:phone
        }).then((response) => {
            setRegisterMessage(response.data.returnMessage);
        });
    }

    return (
        <div className={"App"}>
            <div className={"Login"}>
                <LoginBanner />
                <div className={"login-box"}>
                    <h1 className={"login-title"}>Volunteer Portal</h1>
                    <br/>
                    <TextField style={{backgroundColor:'#fff'}} className={'user-info-input'} id="filled-basic" label="Email" variant="filled" onChange={(e) => {
                        setEmailReg(e.target.value);
                    }} />
                    <br/>
                    <br />
                    <TextField style={{backgroundColor:'#fff'}} className={'user-info-input'} id="filled-basic" label="Password" variant="filled" type={'password'} onChange={(e) => {
                        setPasswordReg(e.target.value);
                    }}/>
                    <br />
                    <br />
                    <TextField style={{backgroundColor:'#fff'}} className={'user-info-input'} id="filled-basic" label="Full Name" variant="filled" onChange={(e) => {
                        setFullName(e.target.value);
                    }}/>
                    <br />
                    <br />
                    <TextField style={{backgroundColor:'#fff'}} className={'user-info-input'} id="filled-basic" label="Prefer Name" variant="filled" onChange={(e) => {
                        setPreferName(e.target.value);
                    }}/>
                    <br />
                    <br />
                    <TextField style={{backgroundColor:'#fff'}} className={'user-info-input'} id="filled-basic" label="Phone Number" variant="filled" onChange={(e) => {
                        setPhone(e.target.value);
                    }}/>
                    <br />
                    <br />
                    <LocalizationProvider dateAdapter={DateAdapter}>
                        <DesktopDatePicker
                            label="Birthday"
                            inputFormat="MM/dd/yyyy"
                            value={value}
                            style={{backgroundColor:'#fff'}}
                            onChange={handleChange}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                    <br />
                    <br />
                    <h1 className={"warnMessage"}>{registerMessage}</h1>
                    <br />
                    <button onClick={register} className={"btn-outline-primary"}>Register</button>
                </div>
            </div>
        </div>
    );
}

export default Register;

import Axios from "axios";
import {useHistory} from "react-router-dom";

export default function UserCheck(){
    return Axios.get('http://localhost:3003/login').then((response) => {
        if(response.data.loggedIn){
            return Axios.get('http://localhost:3003/userVerify', {
                        headers: {
                            "x-access-token": localStorage.getItem("token"),
                        }
                    }).then((res) => {
                        return res.data.auth;
                    })
        }
        return false;
    })
}
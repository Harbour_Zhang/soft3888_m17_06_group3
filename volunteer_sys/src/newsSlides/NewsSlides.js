import React from 'react';
import {Slide} from "react-slideshow-image/lib";
import 'react-slideshow-image/dist/styles.css'

// store the images url
const slideImages = [
    'https://www.vinnies.org.au/scripts/timthumb.php?src=https%3A%2F%2Fwww.vinnies.org.au%2Fcontent%2FImage%2Finternational+active.jpg&w=1440&h=771',
    'https://www.vinnies.org.au/scripts/timthumb.php?src=https%3A%2F%2Fwww.vinnies.org.au%2Fcontent%2FImage%2FNational%2FNews%2FNOSW+pic+1.jpg&w=1440&h=771',
    'https://www.vinnies.org.au/scripts/timthumb.php?src=https%3A%2F%2Fwww.vinnies.org.au%2Fcontent%2FImage%2FNational%2FCOVID-19%2Fbrian-wangenheim-wCqiHkjX1ZU-unsplash.jpg&w=1440&h=771'
];


const NewsSlides = () => {
    return (
        <div>
            {/*each slide setting*/}
            <Slide easing="ease" style={{'paddingRight':`10%`,'paddingLeft':`10%`}}>
                <div className="each-slide" style={{'paddingLeft':`15%`}}>
                    <a href={'https://www.vinnies.org.au/page/Our_Impact/International_Impact/'}>
                        <div style={{'backgroundImage': `url(${slideImages[0]})`, 'height':`771px`, 'width':`1440px`}}>
                            Our Impact
                        </div>
                    </a>
                </div>
                <div className="each-slide" style={{'paddingLeft':`15%`}}>
                    <a href={'https://www.vinnies.org.au/shops'}>
                        <div style={{'backgroundImage': `url(${slideImages[1]})`, 'height':`771px`, 'width':`1440px`}}>
                            Visit Our Shops
                        </div>
                    </a>
                </div>
                <div className="each-slide" style={{'paddingLeft':`15%`}}>
                    <a href={'https://www.vinnies.org.au/page/Find_Help/Covid_19_support/'}>
                        <div style={{'backgroundImage': `url(${slideImages[2]})`, 'height':`771px`, 'width':`1440px`}}>
                            Covid-19 Support
                        </div>
                    </a>
                </div>
            </Slide>
        </div>
    )
};

export default NewsSlides;

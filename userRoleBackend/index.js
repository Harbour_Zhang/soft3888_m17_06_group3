const express = require('express');
const app = express();

const cors = require("cors");
const bcrypt = require("bcrypt");
const saltRounds = 10;

const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const jwt = require("jsonwebtoken");

require('dotenv').config();
const port = process.env.DB_NODE_PORT;

const Pool = require('pg').Pool;
const pool = new Pool({
    user: process.env.DB_USER,
    host: 'db',
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
});

app.use(express.json());
app.use(cors({
    origin: [process.env.DB_ORIGIN],
    methods: ["GET", "POST"],
    credentials: true
}));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: true}));

app.use(session({
    key: process.env.COOKIE_KEY,
    secret: process.env.COOKIE_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires : 60 * 60 * 24 * 1000,
    }
}))

const verifyJwt = (req, res, next) => {
    const token = req.headers["x-access-token"];
    if(!token){
        res.send("We need a token, pls give it to us next time.");
    }else{
        jwt.verify(token, process.env.COOKIE_SECRET, (err, decoded) => {
            if(err){
                res.json({auth:false, message: "Failed auth"})
            }else{
                req.userId = decoded.id;
                next();
            }
        })
    }
}

app.get('/userVerify',verifyJwt , (require, response) => {
    response.json({auth:true, message: "Success auth"});
})

app.get('/login', (require, response) => {
    if(require.session.user){
        response.send({loggedIn:true, user:require.session.user})
    }else{
        response.send({loggedIn: false});
    }
})

app.post('/login', (require, response) => {

    const email = require.body.email;
    const password = require.body.password;

    pool.query('SELECT * FROM user_info WHERE email = $1;', [email], (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                bcrypt.compare(password, result.rows[0].password, (err, res) => {
                    if(res){
                        const id = result.rows[0].userid;
                        const token = jwt.sign({id},"learnfromonline",{
                            expiresIn: 60*60*24*1000,
                        })
                        require.session.user = result;
                        response.json({auth:true, token: token, Id: result.rows[0].userid, result});
                        // console.log(result);
                    }else{
                        response.send({auth:false, incorrect_message: "Wrong password."});
                    }
                })
            }else{
                response.send({auth: false, incorrect_message: "User does not exist."});
            }
        }
    })
})


app.post('/register', (require, response) => {
    const email = require.body.email;
    const password = require.body.password;

    bcrypt.hash(password, saltRounds, (err, hash)=> {
        if(err){
            console.log(err);
        }
        pool.query('INSERT INTO user_info (email, password) VALUES ($1, $2)', [email, hash], (error, result) => {
            if(error){
                console.log(error);
                response.send({returnMessage:"Email has already been used, pls login."});
            }else{
                response.send({returnMessage: "Success registered."});
            }
        })
    })
})

app.post('/registerDetails', (require, response) => {
    const fullName = require.body.fullName;
    const birth = require.body.birth;
    const preferName = require.body.preferName;
    const phoneNumber = require.body.phoneNumber;
    const email= require.body.email;

    pool.query('INSERT INTO normal_user (full_name, birth, preferred_name, phone_number, email) VALUES ($1, $2, $3, $4, $5)', [fullName,birth,preferName,phoneNumber, email], (error, result) => {
        if(error){
            response.send({returnMessage:"Email has already been used, pls login."});
        }else{
            response.send({returnMessage: "Success registered."});
        }
    })

})

app.post('/user', (require, response) => {
    const userId = require.body.userId;

    pool.query('select * from normal_user where userid = $1;', [userId], (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const userId = result.rows[0].userid;
                const teamId = result.rows[0].teamid;
                const roleId = result.rows[0].roleid;
                const fullName = result.rows[0].full_name;
                const birth = result.rows[0].birth;
                const preferredName = result.rows[0].preferred_name;
                const phoneNumber = result.rows[0].phone_number;
                const email = result.rows[0].email;
                const emergencyId = result.rows[0].emergencyid;
                //    analysis data
                response.send({
                    userId:userId,
                    teamId:teamId,
                    roleId:roleId,
                    fullName:fullName,
                    birth:birth,
                    preferredName:preferredName,
                    phoneNumber:phoneNumber,
                    email:email,
                    emergencyId:emergencyId
                });
            }else{
                response.send({auth: false, incorrect_message: "User does not exist."});
            }
        }
    })
})

app.post('/volunteer', (require, response) => {
    const userId = require.body.userId;
    pool.query('select * from volunteer where userId = $1;', [userId], (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const userId = result.rows[0].userid;
                const teamId = result.rows[0].teamid;
                const roleId = result.rows[0].roleid;
                const fullName = result.rows[0].full_name;
                const birth = result.rows[0].birth;
                const preferredName = result.rows[0].preferred_name;
                const phoneNumber = result.rows[0].phone_number;
                const email = result.rows[0].email;
                const emergencyId = result.rows[0].emergencyid;
                const trainingModel = result.rows[0].training_model;
                const wwccNumber = result.rows[0].number_wwcc;

                //    analysis data
                response.send({
                    userId:userId,
                    teamId:teamId,
                    roleId:roleId,
                    fullName:fullName,
                    birth:birth,
                    preferredName:preferredName,
                    phoneNumber:phoneNumber,
                    email:email,
                    emergencyId:emergencyId,
                    trainingModel:trainingModel,
                    wwccNumber:wwccNumber
                });
            }else{
                response.send({auth: false, incorrect_message: "User does not exist."});
            }
        }
    })
})

app.post('/teamCoordinator', (require, response) => {
    const userId = require.body.userId;
    pool.query('select * from team_coordinator where userId = $1;', [userId], (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const userId = result.rows[0].userid;
                const teamId = result.rows[0].teamid;
                const roleId = result.rows[0].roleid;
                const fullName = result.rows[0].full_name;
                const birth = result.rows[0].birth;
                const preferredName = result.rows[0].preferred_name;
                const phoneNumber = result.rows[0].phone_number;
                const email = result.rows[0].email;
                const emergencyId = result.rows[0].emergencyid;
                const wwccNumber = result.rows[0].number_wwcc;

                //    analysis data
                response.send({
                    userId:userId,
                    teamId:teamId,
                    roleId:roleId,
                    fullName:fullName,
                    birth:birth,
                    preferredName:preferredName,
                    phoneNumber:phoneNumber,
                    email:email,
                    emergencyId:emergencyId,
                    wwccNumber:wwccNumber
                });
            }else{
                response.send({auth: false, incorrect_message: "User does not exist."});
            }
        }
    })
})

app.post('/adminCoordinator', (require, response) => {
    const userId = require.body.userId;
    pool.query('select * from admin_coordinator where userId = $1;', [userId], (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const userId = result.rows[0].userid;
                const teamId = result.rows[0].teamid;
                const roleId = result.rows[0].roleid;
                const fullName = result.rows[0].full_name;
                const birth = result.rows[0].birth;
                const preferredName = result.rows[0].preferred_name;
                const phoneNumber = result.rows[0].phone_number;
                const email = result.rows[0].email;
                const emergencyId = result.rows[0].emergencyid;

                //    analysis data
                response.send({
                    userId:userId,
                    teamId:teamId,
                    roleId:roleId,
                    fullName:fullName,
                    birth:birth,
                    preferredName:preferredName,
                    phoneNumber:phoneNumber,
                    email:email,
                    emergencyId:emergencyId
                });
            }else{
                response.send({auth: false, incorrect_message: "User does not exist."});
            }
        }
    })
})

app.post('/teamLeader', (require, response) => {
    const userId = require.body.userId;
    pool.query('select * from team_leader where userId = $1;', [userId], (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const userId = result.rows[0].userid;
                const teamId = result.rows[0].teamid;
                const roleId = result.rows[0].roleid;
                const fullName = result.rows[0].full_name;
                const birth = result.rows[0].birth;
                const preferredName = result.rows[0].preferredname;
                const phoneNumber = result.rows[0].phonenumber;
                const email = result.rows[0].email;
                const emergencyId = result.rows[0].emergencyid;
                const orderId = result.rows[0].orderid;
                const incidentId = result.rows[0].incidentid;
                const trainingModel = result.rows[0].trainingmodel;

                //    analysis data
                response.send({
                    userId:userId,
                    teamId:teamId,
                    roleId:roleId,
                    fullName:fullName,
                    birth:birth,
                    preferredName:preferredName,
                    phoneNumber:phoneNumber,
                    email:email,
                    emergencyId:emergencyId,
                    orderId:orderId,
                    incidentId:incidentId,
                    trainningModel:trainingModel
                });
            }else{
                response.send({auth: false, incorrect_message: "User does not exist."});
            }
        }
    })
})

app.post('/location', (require, response) => {
    const locationId = require.body.locationId;
    pool.query('select * from location where locationId = $1;', [locationId], (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const eventId = result.rows[0].eventid;
                const Num_of_female = result.rows[0].num_of_female;
                const Num_of_male = result.rows[0].num_of_male;
                const Num_of_child = result.rows[0].num_of_child;
                const total = result.rows[0].total;


                //    analysis data
                response.send({
                    eventId:eventId,
                    Num_of_female:Num_of_female,
                    Num_of_male:Num_of_male,
                    Num_of_child:Num_of_child,
                    total:total
                });
            }else{
                response.send({auth: false, incorrect_message: "User does not exist."});
            }
        }
    })
})

app.post('/event', (require, response) => {
    const eventId = require.body.eventId;
    pool.query('select * from event where eventId = $1;', [eventId], (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const teamID = result.rows[0].teamid;
                const incidentId = result.rows[0].incidentid;
                const team_name = result.rows[0].team_name;
                const team_night = result.rows[0].team_night;
                const start_time = result.rows[0].start_time;
                const end_time = result.rows[0].end_time;
                const date = result.rows[0].date;
                const kitchen_condition_report = result.rows[0].kitchen_condition_report;
                const Vehicle_condition_report = result.rows[0].vehicle_condition_report;
                const Night_comment = result.rows[0].night_comment;



                //    analysis data
                response.send({
                    teamID:teamID,
                    incidentId:incidentId,
                    team_name:team_name,
                    team_night:team_night,
                    start_time:start_time,
                    end_time:end_time,
                    date:date,
                    kitchen_condition_report:kitchen_condition_report,
                    Vehicle_condition_report:Vehicle_condition_report,
                    Night_comment:Night_comment
                });
            }else{
                response.send({auth: false, incorrect_message: "User does not exist."});
            }
        }
    })
})

app.post('/team', (require, response) => {
    const teamId = require.body.teamId;
    pool.query('select * from team where teamId = $1;', [teamId], (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const teamName = result.rows[0].teamname;
                const teamLeaderId = result.rows[0].teamleaderid;
                const eventId = result.rows[0].eventid;
                const teamCoordinatorId = result.rows[0].teamcoordinatorid;
                const contactInformation = result.rows[0].contactinformation;

                //    analysis data
                response.send({
                    teamName:teamName,
                    teamLeaderId:teamLeaderId,
                    eventId:eventId,
                    teamCoordinatorId:teamCoordinatorId,
                    contactInformation:contactInformation
                });
            }else{
                response.send({auth: false, incorrect_message: "User does not exist."});
            }
        }
    })
})

app.post('/incident', (require, response) => {
    const incidentId = require.body.incidentId;
    pool.query('select * from incident where incidentId = $1;', [incidentId], (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const userID = result.rows[0].userid;
                const status = result.rows[0].status;
                const time = result.rows[0].time;
                const date = result.rows[0].date;
                const engaged_activity = result.rows[0].engaged_activity;
                const location = result.rows[0].location;
                const description = result.rows[0].description;
                const recipient = result.rows[0].recipient;
                const reporter = result.rows[0].reporter;
                const reported_time = result.rows[0].reported_time;
                const reported_date = result.rows[0].reported_date;

                //    analysis data
                response.send({
                    userID:userID,
                    status:status,
                    time:time,
                    date:date,
                    engaged_activity:engaged_activity,
                    location:location,
                    description:description,
                    recipient:recipient,
                    reporter:reporter,
                    reported_time:reported_time,
                    reported_date:reported_date
                });
            }else{
                response.send({auth: false, incorrect_message: "User does not exist."});
            }
        }
    })
})

app.post('/normal_order', (require, response) => {
    const orderId = require.body.orderId;
    pool.query('select * from normal_order where orderId = $1;', [orderId], (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const status = result.rows[0].status;
                const userID = result.rows[0].userid;
                const homeless_record_id = result.rows[0].homeless_record_id;
                const blanket_number = result.rows[0].blanket_number;
                const bags_number = result.rows[0].bags_number;
                const ordered_date = result.rows[0].ordered_date;
                const received_date = result.rows[0].received_date;
                const cloth = result.rows[0].cloth;
                const special_request = result.rows[0].special_request;
                const comments = result.rows[0].comments;
                //    analysis data
                response.send({
                    userID:userID,
                    status:status,
                    homeless_record_id:homeless_record_id,
                    blanket_number:blanket_number,
                    bags_number:bags_number,
                    ordered_date:ordered_date,
                    received_date:received_date,
                    special_request:special_request,
                    comments:comments

                });
            }else{
                response.send({auth: false, incorrect_message: "User does not exist."});
            }
        }
    })
})

app.post('/homeless_record', (require, response) => {
    const recordID = require.body.recordId;
    pool.query('select * from homeless_record where recordID = $1;', [recordID], (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const legal_name = result.rows[0].legal_name;
                const incidentID = result.rows[0].incidentid;
                const street_name = result.rows[0].street_name;
                const birth_year = result.rows[0].birth_year;
                const registered_date = result.rows[0].registered_date;
                const blanket_number = result.rows[0].blanket_number;
                const adminid = result.rows[0].adminid;
                const bags_number = result.rows[0].bags_number;


                //    analysis data
                response.send({
                    legal_name:legal_name,
                    incidentID:incidentID,
                    street_name:street_name,
                    birth_year:birth_year,
                    registered_date:registered_date,
                    blanket_number:blanket_number,
                    bags_number:bags_number,
                    adminid:adminid
                });
            }else{
                response.send({auth: false, incorrect_message: "User does not exist."});
            }
        }
    })
})

app.post('/insert_to_volunteer', (require, response) => {
    const userId = require.body.userId;
    pool.query('INSERT INTO user_login_info (userId) VALUES ($1)', [userId], (error, result) => {
        if(error){
            response.send({returnMessage:"Fail add user in volunteer repo"});
        }else{
            response.send({returnMessage: "Success add user in volunteer repo"});
        }
    })
})


app.post('/role', (require, response) => {
    const roleId = require.body.roleId;
    pool.query('select roleName from role where roleID = $1;', [roleId], (error, result) => {
        if (error){
            response.send({error:error});
        }
        else{
            if(result.rowCount > 0){
                const roleName = result.rows[0].rolename;
                response.send({roleName:roleName});
            }
            else{
                response.send({incorrect_message:"role not exists"});
            }
        }
    })
})

app.post('/emergencyContactRole', (require, response) => {
    const userId = require.body.userId;
    pool.query('select * from emergency_contactRole where emergencyId = (select emergencyid from normal_user where userId = $1);', [userId], (error, result) => {
        if (error){
            response.send({error:error});
        }
        else{
            if(result.rowCount > 0){
                const userId = result.rows[0].userid;
                const name = result.rows[0].name;
                const familyRole = result.rows[0].familyrole;
                const phoneNumber = result.rows[0].phone_number;

                response.send({
                    userId:userId,
                    name:name,
                    familyRole:familyRole,
                    phoneNumber:phoneNumber
                });
            }
            else{
                response.send({incorrect_message:"emergency contact role not exists"});
            }
        }
    })
})



app.post('/wwccNumber', (require, response) => {
    const wwcc = require.body.wwccNumber;
    pool.query('select * from wwcc_number where number = $1;', [wwcc], (error, result) => {
        if (error){
            response.send({error:error});
        }
        else{
            if(result.rowCount > 0){
                const APP_number = result.rows[0].app_number;
                const type = result.rows[0].type;
                const expiry_date = result.rows[0].expiry_date;
                const valid_status = result.rows[0].valid_status;

                response.send({
                    APP_number:APP_number,
                    type:type,
                    expiry_date:expiry_date,
                    valid_status:valid_status
                });
            }
            else{
                response.send({incorrect_message:"wwcc number not exists"});
            }
        }
    })
})

app.post('/volunteer/wwccNumber', (require, response) => {
    const volunteerId = require.body.volunteerId;
    pool.query('select wwcc_number.* from volunteer left outer join wwcc_number on volunteer.number_wwcc=wwcc_number.number where volunteer.userid = $1;', [volunteerId], (error, result) => {
        if (error){
            response.send({error:error});
        }
        else{
            if(result.rowCount > 0){
                const number = result.rows[0].number;
                const APP_number = result.rows[0].app_number;
                const type = result.rows[0].type;
                const expiry_date = result.rows[0].expiry_date;
                const valid_status = result.rows[0].valid_status;

                response.send({
                    number:number,
                    APP_number:APP_number,
                    type:type,
                    expiry_date:expiry_date,
                    valid_status:valid_status
                });
            }
            else{
                response.send({incorrect_message:"wwcc number not exists"});
            }
        }
    })
})



app.post('/teamCoordinator/addTeamMemberById', (require, response) => {
    //const id = require.body.teamCoordinatorId;
    const teamId = require.body.teamId;
    const memberId = require.body.userId;

    pool.query('update normal_user set teamid = array_append((select teamid from normal_user where userid = $2),$1) where userid = $2 and not $1 = any (teamid);' +
        'update volunteer set teamid = (select teamid from normal_user where userid = $2) where userid = $2',
        [teamId,memberId], (error, result) => {
            if (error){
                response.send({error:error});
            }
            else{
                if(result.rowCount >= 0){
                    response.send("success to add team member");
                }
                else{
                    response.send({incorrect_message:"fail to add team member"});
                }
            }
        })
})

app.post('/teamCoordinator/addTeamMemberByName', (require, response) => {
    // const id = require.body.teamCoordinatorId;
    const teamId = require.body.teamId;
    const name =  require.body.fullName;
    pool.query('update normal_user set teamid = array_append((select teamid from normal_user where full_name = $2),$1) where full_name = $2 and not $1 = any (teamid)', [teamId,name], (error, result) => {
        if (error){
            response.send({error:error});
        }
        else{
            if(result.rowCount >=0){
                response.send("success to add team member");
            }
            else{
                response.send({incorrect_message:"fail to add team member"});
            }
        }
    })
})

// get all team members for team coordinator page
app.post('/getTeamMember', (require, response) => {
    const teamId = require.body.teamId;
    pool.query('select * from volunteer where $1 = any(teamid)',[teamId],(error,result)=>{
        if (error){
            response.send({error:error});
        }
        else{
            if (result.rowCount > 0){
                const allrows = result.rows;
                response.send({
                    allrows:allrows
                });
            }
            else{
                response.send({incorrect_message:"fail to find team member"});
            }
        }
    })

})

// get contacts full name for team coordinator page
app.get('/teamCoordinator/getContacts', (require, response) => {
    pool.query('select * from team_coordinator', (error, result) => {
        if (error){
            response.send({error:error});
        }
        else{
            if(result.rowCount > 0){
                const allrows = result.rows;

                response.send({
                    allrows:allrows
                });
            }
            else{
                response.send({incorrect_message:"fail to add team member"});
            }
        }
    })
})

// get all incidents for team coordinator page
app.post('/teamCoordinator/getAllIncidents', (require, response) => {
    const teamCordId = require.body.teamCoordinatorId;
    pool.query('select * from incident where userid in (select userid from team_leader where teamid && (select teamid from team_coordinator where userid = $1))',[teamCordId],(error, result)=>{
        if (error){
            response.send({error:error});
        }
        else{
            if (result.rowCount>0){
                const allRows = result.rows;
                response.send({
                    allRows:allRows
                });
            }
        }

    })
})

// get all orders for team coordinator page
app.post('/teamCoordinator/getAllOrders' , (require, response) => {
    const teamCordId = require.body.teamCoordinatorId;
    pool.query('select * from normal_order where userid in (select userid from team_leader where teamid && (select teamid from team_coordinator where userid = $1))', [teamCordId], (error, result) => {
        if (error){
            response.send({error:error});
        }
        else{
            if (result.rowCount>0){
                const allRows= result.rows;
                response.send({
                    allRows:allRows
                });

            }
        }

    })

})

//get all attend history for volunteer page.
app.post('/volunteer/attendHistory', (require, response) => {
    const volunteerid = require.body.volunteerId;
    pool.query('select event.team_name, event.team_night, event.vehicle_condition_report , event.start_time, event.end_time from volunteer inner join event on event.teamid = any (volunteer.teamid) where volunteer.userid = $1', [volunteerid], (error, result) => {
        if (error){
            response.send({error:error});
        }
        else{
            if(result.rowCount > 0){
                const allRows= result.rows;
                response.send({
                    allRows:allRows

                });
            }
            else{
                response.send({incorrect_message:"fail to add team member"});
            }
        }
    })
})

app.post('/teamLeader/addTeamToEvent',(require,response)=>{
    const userid = require.body.userid;
    const eventid = require.body.eventid;
    pool.query('update event set volunteerid = array_append((select volunteerid from event where eventid = $2),$1) ' +
        'where eventid = $2 and not $1 = any(volunteerid)',[userid,eventid],(error, result) => {
        if (error){
            response.send({error:error});
        }
        else{
            response.send({result,result});
        }
    })
})

app.post('/teamLeader/createIncident',(require,response)=>{

    const teamLeaderId = require.body.teamLeaderId;
    // const userid = require.body.userid;
    const status = 'unfinished';
    const engagedActivity = require.body.engagedActivity;
    const location = require.body.location;
    const recipient = require.body.recipient;
    const reporter = require.body.reporter;
    const date = require.body.date;
    const time = require.body.time;
    const reportedTime = require.body.reportedTime;
    const reportedDate = require.body.reportedDate;
    const desc = "None";
    const event_name = require.body.event_name;

    pool.query('insert into incident (userid, status, time,date, engaged_activity,location,description, recipient, reporter,reported_time, reported_date )' +
        ' values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11);',[teamLeaderId,status,time, date, engagedActivity,location,desc,recipient,reporter,reportedTime,reportedDate],(error, result) => {
        if (error){
            response.send({error:error});
        }
        else{
            pool.query('UPDATE event  \n' +
                'SET incidentid = \n' +
                'CASE incidentid  \n' +
                'WHEN null THEN array[(select incidentid from incident order by incidentid desc limit 1)]\n' +
                'else  array_append((select incidentid from event where event_name = $1),(select incidentid from incident order by incidentid desc limit 1))  \n' +
                'END \n' +
                'where event_name= $1 and not  (select incidentid from incident order by incidentid desc limit 1)= any(incidentid);',[event_name])
            response.send({result,result});
        }
    })
})

app.post('/teamLeader/createOrder',(require,response)=>{

    const blank = require.body.blank;
    const bag = require.body.bag;
    const orderedDate = require.body.orderedDate;
    const receivedDate = require.body.receivedDate;
    const request = require.body.request;
    const comment = require.body.comment;
    const status = require.body.status;
    const userid = require.body.userid;
    const hls = require.body.homelessid;
    const cloth = require.body.cloth;


    pool.query('insert into normal_order (status, userid, blanket_number,bags_number,ordered_date,received_date,special_request,comments,homeless_record_id,cloth ) ' +
        'values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)',[status,userid,blank,bag,orderedDate,receivedDate,request,comment,hls,cloth],(error, result) => {        if (error){
        response.send({error:error});
    }
    else{
        response.send({result,result});
    }
    })
})



app.post('/teamLeader/getEmergencyId',(require,response)=>{
    const userid = require.body.userid;
    pool.query('select e.* from volunteer join emergency_contactrole e using (emergencyid)\n' +
        'where userid = $1; ',[userid],(error, result) => {
        if (error){
            response.send({error:error});
        }
        else{
            if (result.rowCount>0){
                const name = result.rows[0].name;
                const phone_number = result.rows[0].phone_number;
                const familyrole = result.rows[0].familyrole;
                response.send({
                    name:name,
                    familyrole:familyrole,
                    phone_number:phone_number
                });
            }
        }
    })
})

app.post('/teamLeader/getEventId',(require,response)=>{
    const eventid = require.body.eventid;
    pool.query('select * from event \n' +
        'where eventid = $1; ',[eventid],(error, result) => {
        if (error){
            response.send({error:error});
        }
        else{
            if (result.rowCount>0){
                const start_time = result.rows[0].start_time;
                const end_time = result.rows[0].end_time;
                const date = result.rows[0].date;
                const vehicle_condition_report = result.rows[0].vehicle_condition_report;
                const kitchen_condition_report = result.rows[0].kitchen_condition_report;
                const night_comment = result.rows[0].night_comment;

                response.send({
                    start_time:start_time,
                    end_time:end_time,
                    date:date,
                    vehicle_condition_report:vehicle_condition_report,
                    kitchen_condition_report:kitchen_condition_report,
                    night_comment:night_comment
                });
            }
        }
    })
})


app.post('/homeless_record', (require, response) => {
    const recordID = require.body.recordId;
    pool.query('select * from homeless_record where recordID = $1;', [recordID], (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const legal_name = result.rows[0].legal_name;
                const incidentID = result.rows[0].incidentid;
                const street_name = result.rows[0].street_name;
                const birth_year = result.rows[0].birth_year;
                const registered_date = result.rows[0].registered_date;
                const blanket_number = result.rows[0].blanket_number;
                const adminid = result.rows[0].adminid;
                const bags_number = result.rows[0].bags_number;


                //    analysis data
                response.send({
                    legal_name:legal_name,
                    incidentID:incidentID,
                    street_name:street_name,
                    birth_year:birth_year,
                    registered_date:registered_date,
                    blanket_number:blanket_number,
                    bags_number:bags_number,
                    adminid:adminid
                });
            }else{
                response.send({auth: false, incorrect_message: "User does not exist."});
            }
        }
    })
})


/* From here is the function needed for admin role.  */

//  Return a list of all people inside the system
app.get('/view_people', (require, response) => {
    console.log("viewing");
    pool.query('select * from normal_user', (error, result) => {
        if(error){
            console.log("query error");
            response.send({error:error});
        }else{
            console.log("success");
            if (result.rowCount > 0){
                const userList = result.rows;
                response.send({
                    userList:userList,
                });
            }else{
                response.send({exists: false, incorrect_message: "No user in database"});
            }
        }
    })
})


//  Return a list of all people inside the system
app.post('/personal_info', (require, response) => {
    const user = require.body.user;
    pool.query('select * from normal_user where full_name = $1', [user], (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                // console.log(user);
                const userid = result.rows[0].userid;
                const teamid = result.rows[0].teamid;
                const roleid = result.rows[0].userid;
                const full_name = result.rows[0].full_name;
                const birth = result.rows[0].birth;
                const preferred_name = result.rows[0].preferred_name;
                const phone_number = result.rows[0].phone_number;
                const email = result.rows[0].email;

                response.send({
                    userid:userid,
                    teamid:teamid,
                    roleid:roleid,
                    full_name:full_name,
                    birth:birth,
                    preferred_name:preferred_name,
                    phone_number:phone_number,
                    email:email
                });
            }else{
                response.send({exists: false, incorrect_message: "No user in database"});
            }
        }
    })
})

//  Return a list of all people inside the system
app.post('/view_order_submitted', (require, response) => {
    pool.query("select * from normal_order WHERE status='submitted'", (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const orderList = result.rows;
                response.send({
                    orderList:orderList,
                });
            }else{
                response.send({exists: false, incorrect_message: "No order in database"});
            }
        }
    })
})

//  Return a list of all people inside the system
app.post('/view_order_processing', (require, response) => {
    pool.query("select * from normal_order WHERE status='processing'", (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const orderList = result.rows;
                response.send({
                    orderList:orderList,
                });
            }else{
                response.send({exists: false, incorrect_message: "No order in database"});
            }
        }
    })
})

//  Return a list of all people inside the system
app.post('/view_order_approved', (require, response) => {
    pool.query("select * from normal_order WHERE status='approved'", (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const orderList = result.rows;
                response.send({
                    orderList:orderList,
                });
            }else{
                response.send({exists: false, incorrect_message: "No order in database"});
            }
        }
    })
})

//  Return a list of all people inside the system
app.post('/view_order_rejected', (require, response) => {
    pool.query("select * from normal_order WHERE status='rejected'", (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const orderList = result.rows;
                response.send({
                    orderList:orderList,
                });
            }else{
                response.send({exists: false, incorrect_message: "No order in database"});
            }
        }
    })
})

//  Return a list of all people inside the system
app.post('/order_info', (require, response) => {
    const id = require.body.id;
    pool.query('select * from normal_order where orderid = $1', [id], (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const orderid = result.rows[0].orderid;
                const status = result.rows[0].status;
                const userid = result.rows[0].userid;
                const homeless_record_id = result.rows[0].homeless_record_id;
                const blanket_number = result.rows[0].blanket_number;
                const bags_number = result.rows[0].bags_number;
                const ordered_date = result.rows[0].ordered_date;
                const received_date = result.rows[0].received_date;
                const cloth = result.rows[0].cloth;
                const special_request = result.rows[0].special_request;
                const comments = result.rows[0].comments;

                response.send({
                    orderid: orderid,
                    status: status,
                    userid: userid,
                    homeless_record_id: homeless_record_id,
                    blanket_number: blanket_number,
                    bags_number: bags_number,
                    ordered_date: ordered_date,
                    received_date: received_date,
                    cloth: cloth,
                    special_request: special_request,
                    comments: comments
                });
            }else{
                response.send({exists: false, incorrect_message: "No order in database"});
            }
        }
    })
})


app.get('/admin/wwccNumber', (require, response) => {
    pool.query('select wwcc_number.*, volunteer.* from volunteer left outer join wwcc_number on volunteer.number_wwcc=wwcc_number.number where valid_status=0', (error, result) => {
        if (error){
            response.send({error:error});
        }
        else{
            if(result.rowCount > 0){
                const wwccList = result.rows;
                response.send({
                    wwccList: wwccList
                });
            }
            else{
                response.send({incorrect_message:"wwcc number not exists"});
            }
        }
    })
})



//update WWCC number valid status to 1 for admin page
app.post('/admin/confirmWWCCStatus', (require, response) => {
    const wwccNumber = require.body.wwccNumber;
    // console.log(wwccNumber)
    pool.query('UPDATE wwcc_number SET valid_status = 1 where number = $1', [wwccNumber], (error, result) => {
        if (error){
            response.send({error:error});
        }else{
            pool.query('select wwcc_number.*, volunteer.* from volunteer left outer join wwcc_number on volunteer.number_wwcc=wwcc_number.number where valid_status=0', (error, result) => {
                if (error){
                    response.send({error:error});
                }
                else{
                    if(result.rowCount > 0){
                        const wwccList = result.rows;
                        response.send({
                            wwccList: wwccList
                        });
                    }
                    else{
                        response.send({incorrect_message:"wwcc number not exists"});
                    }
                }
            })
        }
    })
})


//update WWCC number valid status to 1 for admin page
app.get('/admin/homelessCount', (require, response) => {
   pool.query('select COUNT(recordID) from homeless_record', (error, result) => {
        if (error){
            response.send({error:error});
        }
        else{
            const counts = result.rows[0];
            response.send({
                counts: counts
            });
        }
    })
})

// add new homeless record for admin coordinator page
app.post('/adminCoordinator/addNewRecord', (require, response) => {
    const adminId = require.body.adminId;
    const legal_name =require.body.legal_name ;
    const streetname = require.body.streetname;
    const birth_year = require.body.birth_year;
    const registered_date = require.body.registered_date;
    console.log("adding");

    pool.query('insert into homeless_record(legal_name,incidentid,street_name,birth_year,registered_date,blanket_number, bags_number, adminid) values($2,null,$3,$4,$5,0,0,$1)',[adminId,legal_name,streetname,birth_year,registered_date],(error, result)=>{
        if (error){
            response.send({error:error});
        }
    })

})


app.post('/adminCoordinator', (require, response) => {
    const userId = require.body.userId;
    pool.query('select * from admin_coordinator where userId = $1;', [userId], (error, result) => {
        if(error){
            response.send({error:error});
        }else{
            if (result.rowCount > 0){
                const userId = result.rows[0].userid;
                const teamId = result.rows[0].teamid;
                const roleId = result.rows[0].roleid;
                const fullName = result.rows[0].full_name;
                const birth = result.rows[0].birth;
                const preferredName = result.rows[0].preferred_name;
                const phoneNumber = result.rows[0].phone_number;
                const email = result.rows[0].email;
                const emergencyId = result.rows[0].emergencyid;

                //    analysis data
                response.send({
                    userId:userId,
                    teamId:teamId,
                    roleId:roleId,
                    fullName:fullName,
                    birth:birth,
                    preferredName:preferredName,
                    phoneNumber:phoneNumber,
                    email:email,
                    emergencyId:emergencyId
                });
            }else{
                response.send({auth: false, incorrect_message: "User does not exist."});
            }
        }
    })
})

//update WWCC number valid status to 1 for admin page
app.get('/admin/view_record', (require, response) => {
    pool.query('SELECT * FROM homeless_record', (error, result) => {
        if (error){
            response.send({error:error});
        }else{
            const recordList = result.rows;
            // console.log(recordList)
            response.send({
                recordList: recordList
            });
        }
    })
})

app.get('/teamLeader/getAllTeams', (require, response) => {
    pool.query('SELECT * FROM team', (error, result) => {
        if (error){
            response.send({error:error});
        }else{
            const recordList = result.rows;
            // console.log(recordList);
            response.send({
                recordList: recordList
            });
        }
    })
})

app.get('/allDrivers', (require, response) =>{
    pool.query('select distinct driver from event', (error, result) =>{
        if (error) {
            response.send({error:error});
        }
        else{
            const list = result.rows;
            response.send({
                list:list
            });
        }
    })
})

app.get('/allVolunteers', (require, response) =>{
    pool.query('select userid, roleid,teamid, full_name,birth, e.phone_number,\n' +
        'case \n' +
        '\twhen v.training_model = 0 then \'not finished\'\n' +
        '\twhen v.training_model = 1 then \'finished\'\n' +
        'end as training_model\n' +
        'from volunteer v left join emergency_contactrole e using(emergencyid)  ;', (error, result) =>{
        if (error) {
            response.send({error:error});
        }
        else{
            const list = result.rows;
            response.send({
                list:list
            });
        }
    })
})

app.post('/teamLeader/createEvent', (require, response) =>{
    const teamid = require.body.teamid;
    const team_name = require.body.team_name;
    const team_night = require.body.team_night;
    const start_time = require.body.start_time;
    const end_time = require.body.end_time;
    const date = require.body.date;
    const volunteerid = require.body.volunteerid;
    const night_comment = require.body.night_comment;
    const driver = require.body.driver;
    const event_name = require.body.event_name;
    pool.query('insert into event (teamid, team_name, team_night, start_time,end_time,date,volunteerid,night_comment,driver,event_name)' +
        'values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)', [teamid, team_name, team_night, start_time,end_time,date,volunteerid,night_comment,driver,event_name],  (error, result) =>{
        if (error){
            response.send({error:error});
        }
    });
})


app.get('/teamLeader/getAllEvent', (require, response) =>{
    pool.query('select event_name from event where event_name is not null', (error ,result)=>{
        if (error){
            response.send({error:error});
        }
        else{
            response.send({list: result.rows});

        }
    })
})

app.post('/teamLeader/finishedTraining',(require,response) =>{
    const teamLeaderId = require.body.teamid;

    pool.query('update team_leader set training_model = 1 where userid = $1',[teamLeaderId],(error, result) =>{
        if (error){
            response.send({error:error});
        }
    })
})

app.post('/volunteer/finishedTraining',(require,response) =>{
    const teamLeaderId = require.body.volunteerId;

    pool.query('update volunteer set training_model = 1 where userid = $1',[teamLeaderId],(error, result) =>{
        if (error){
            response.send({error:error});
        }
    })
})


app.post('/teamLeader/getTrainingStatus',(require,response) =>{
    const teamLeaderId = require.body.teamid;

    pool.query('select training_model from team_leader where userid = $1',[teamLeaderId],(error, result) =>{
        if (error){
            response.send({error:error});
        }
        else{
            if (result.rowCount > 0){
                response.send({
                    status: result.rows[0].training_model
                });
            }
            else{
                response.send({incorrect_message :'team leader not exists'});
            }

        }
    })
})
/* Admin role functions ended  */

app.post('/userBasicInfoEdit', (require, response) => {
    const userId = require.body.userId;
    const preferredName = require.body.preferredName;
    const email = require.body.email;
    const phoneNumber = require.body.phoneNumber;
    const birth = require.body.birth;
    const roleId = require.body.roleId;
    // const ECRName = require.body.ECRName;
    // const familyRole = require.body.familyrole;
    // const ECRPhoneNumber = require.body.ECRPhoneNumber;
    pool.query('UPDATE normal_user SET preferred_name = $2 , email = $3, phone_number = $4, birth = $5 ,roleId=$6,emergencyid = (SELECT emergencyid FROM emergency_contactrole ORDER BY emergencyid DESC LIMIT 1)where userId = $1;', [userId,preferredName,email,phoneNumber,birth,roleId], (error, result) => {
        if (error){
            response.send({error:error});
        }else{
            response.send("success to edit");
        }
    })
})

app.post('/editEmergencyContactRole', (require, response) => {
    const ECRName = require.body.ECRName;
    const familyRole = require.body.familyRole;
    const ECRPhoneNumber = require.body.ECRPhoneNumber;
    pool.query('insert into emergency_contactrole (name,familyrole,phone_number)values($1,$2,$3);', [ECRName,familyRole,ECRPhoneNumber], (error, result) => {
        if (error){
            response.send({error:error});
        }else{
            response.send("success to edit");
        }
    })
})



app.post('/BasicInfoEdit', (require, response) => {
    const userId = require.body.userId;
    const preferredName = require.body.preferredName;
    const email = require.body.email;
    const phoneNumber = require.body.phoneNumber;
    const birth = require.body.birth;
    const wwccNumber = require.body.wwccNumber;
    const roleId = require.body.roleId;
    pool.query('UPDATE volunteer SET preferred_name = $2 , email = $3, phone_number = $4, birth = $5 ,number_wwcc= $6 , roleId=$7,emergencyid = (SELECT emergencyid FROM emergency_contactrole ORDER BY emergencyid DESC LIMIT 1)where userId = $1', [userId,preferredName,email,phoneNumber,birth,wwccNumber,roleId], (error, result) => {
        if (error){
            response.send({error:error});
        }else{
            response.send("success to edit");
        }
    })
})
app.post('/createWWCCNumber', (require, response) => {
    const number = require.body.wwccNumber;
    const app_number = require.body.app_number;
    const type = require.body.type;
    const expiry_date = require.body.expiry_date;
    pool.query('INSERT INTO wwcc_number VALUES ($1,$2,$3,$4,0);', [number,app_number,type,expiry_date], (error, result) => {
        if (error){
            response.send({error:error});
        }
        else{
            response.send("success to create")
        }
    })
})

app.post('/TeamCoordinatorEmergencyContactRole', (require, response) => {
    const userId = require.body.userId;
    pool.query('select * from emergency_contactRole where emergencyId = (select emergencyid from team_coordinator where userId = $1);', [userId], (error, result) => {
        if (error){
            response.send({error:error});
        }
        else{
            if(result.rowCount > 0){
                const userId = result.rows[0].userid;
                const name = result.rows[0].name;
                const familyRole = result.rows[0].familyrole;
                const phoneNumber = result.rows[0].phone_number;

                response.send({
                    userId:userId,
                    name:name,
                    familyRole:familyRole,
                    phoneNumber:phoneNumber
                });
            }
            else{
                response.send({incorrect_message:"emergency contact role not exists"});
            }
        }
    })
})

app.post('/emergencyContactRoleInAccount', (require, response) => {
    const userId = require.body.userId;
    pool.query('select * from emergency_contactRole where emergencyId = (select emergencyid from volunteer where userId = $1);', [userId], (error, result) => {
        if (error){
            response.send({error:error});
        }
        else{
            if(result.rowCount > 0){
                const userId = result.rows[0].userid;
                const name = result.rows[0].name;
                const familyRole = result.rows[0].familyrole;
                const phoneNumber = result.rows[0].phone_number;

                response.send({
                    userId:userId,
                    name:name,
                    familyRole:familyRole,
                    phoneNumber:phoneNumber
                });
            }
            else{
                response.send({incorrect_message:"emergency contact role not exists"});
            }
        }
    })
})



app.listen(port, () => {
    console.log(`App running on port ${port}.`)
})
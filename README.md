# README #


## What is this repository for? ###

* CP4 - Volunteer Management System
* SOFT3888_M17_06_Group3


## How do I get set up? ###

### Set up webpages and database

1. Modify the variables in the `.env` file
* The origin variable should be the backend host + backend port
2. Run command: 
`bash scripts/install-app.sh` This command will setup the database and build docker images.

## How to run?
Run the bash scripts to start database and react js by command: `bash scripts/start-app.sh`

Run the bash scripts to stop database and react js by command: `bash scripts/stop-app.sh`

[comment]: <> (* Go to http://localhost:3000/register to register you first account &#40;remember this&#41;)

[comment]: <> (* Login with this account)

[comment]: <> (* I will introduce a demo path:)

[comment]: <> (  * You will be redirected to the team coordinator page, this because we set the default role for the first account to team coordinator)

[comment]: <> (  * We currently not allow changing roles because we haven't get the files from client which contains all volunteers' info)

[comment]: <> (* To visit other pages, you can directly input the address in address bar)

## Error Fixing
1. Error: port has been used
   1. Option 1
      1. sudo lsof -i :port
      2. sudo kill -9 <PID>
   2. Option 2
      1. Mac OS
         1. cd /Library/LaunchDaemons/ 
         2. sudo rm com.edb.launchd.postgresql-'version'.plist
      2. Linux
         1. systemctl stop postgresql
2. Error: container unhealthy
   1. Rerun install-app.sh

## Database Structure

* The normal_user table stores all users basic information, for more detailed information
  related to each role, access admin_coordinator, team_coordinator, team_leader and volunteer table.

* The basic information stored in normal_user table is userId, teamId, roleId, fullName, birth, perferredName, phoneNumber, email, emergencyid.

* For admin coordinator table has recordId attribute

* For team coordinator table has same attributes as normal_user.

* For team leader table has training model, orderId, incidentId and eventId attributes except the basic information.

* For volunteer table has training model attributes except the basic information.

Userid is the primary key for all these five tables 

* ps: for orderId, recordId, incidentId, eventId, teamid, teamleaderid, teamcoordinatorid, roleid, volunteerid attribute in each table, the data type is list of integer.

* pps: 
  * roleId:
  * 1.Volunteer 
  * 2.Admin 
  * 3.Team Coordinator 
  * 4.Team Leader

### ERD diagram

* https://lucid.app/lucidchart/invitations/accept/inv_73ac77f5-477d-42dd-a116-510af542be08?viewport_loc=-1124%2C-728%2C4263%2C2459%2C0_0
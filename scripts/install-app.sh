#!/bin/sh

#copy the configuration file
cp .env ./userRoleBackend/.env
cp .env ./volunteer_sys/.env
cp .env ./databaseConstruct/.env

#build the express backend image
cd userRoleBackend;
docker build -t volunteer-backend .;
echo "beck end express container build";

#build the react front end
cd ..;
cd volunteer_sys;
docker build -t volunteer-frontend .;
echo "front end react container build";

#construct the database
cd ..;
cd databaseConstruct;
docker pull postgres;
docker build -t db-construct .;
docker volume create pgdata;

docker-compose up -d;

#Check if the data insert process is done.
echo "Database constructing, please wait...";
while [ "$(docker ps -q -f name=node-construct)" ]
do
  sleep 10;
done

echo "Database construct finished";
docker-compose down;
echo "Database construct container removed";
#!/bin/sh

cp .env ./userRoleBackend/.env
cp .env ./volunteer_sys/.env
cp .env ./databaseConstruct/.env

docker-compose up -d;
